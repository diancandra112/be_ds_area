const jwt = require("jsonwebtoken");
const models = require("../models/index");
const moment = require("moment");
require("dotenv").config();
module.exports = {
  AuthSADanAdmin: async (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      let decode = await jwt.verify(token, process.env.jwt_secret);
      let user = await models.ds_user.findOne({
        where: { id: decode.id },
        // attributes: ["id", "role_id"],
      });
      user = JSON.parse(JSON.stringify(user));
      if (![1, 2].includes(user.role_id)) {
        throw new Error("Invalid Role");
      }
      await models.ds_user.update(
        { last_access: moment().format("YYYY-MM-DD HH:mm:ss") },
        {
          where: { id: decode.id },
        }
      );
      req.decoded = user;
      next();
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: error.message,
      });
    }
  },
  AuthAll: async (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      let decode = await jwt.verify(token, process.env.jwt_secret);
      let user = await models.ds_user.findOne({
        where: { id: decode.id },
        // attributes: ["id", "role_id"],
      });
      user = JSON.parse(JSON.stringify(user));
      await models.ds_user.update(
        { last_access: moment().format("YYYY-MM-DD HH:mm:ss") },
        {
          where: { id: decode.id },
        }
      );

      req.decoded = user;
      next();
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: error.message,
      });
    }
  },
};
