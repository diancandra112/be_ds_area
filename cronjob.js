const corn = require("node-cron");
const moment = require("moment");
const models = require("./models/index");
const fs = require("fs");
const delete_file = () => {
  const job = corn.schedule("1 */1 * * *", async () => {
    //   const job = corn.schedule("* * * * * *", async () => {
    try {
      job.stop();
      console.log("delete_file");
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      let file = await models.sequelize.query(`
      SELECT * FROM ds_temp_files as a WHERE TIMESTAMPDIFF(HOUR,a.time,'${now}')>4
      `);
      file = file[0];
      for (let i = 0; i < file.length; i++) {
        const element = file[i];
        try {
          await fs.unlinkSync(element.path);
        } catch (error) {}
        await models.ds_temp_file.destroy({ where: { id: element.id } });
      }
      console.log("done");
      job.start();
    } catch (error) {
      console.log(error);
      job.start();
    }
  });
};
module.exports = {
  cronjob: () => {
    // delete_file();
  },
};
