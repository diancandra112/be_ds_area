var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const cors = require("cors");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var uploadRouter = require("./routes/upload");
var bannerRouter = require("./routes/banner");
var soalRouter = require("./routes/soal");
var assetRouter = require("./routes/assetRead");
var kelasRouter = require("./routes/kelas");
var voucherRouter = require("./routes/voucher");
var paymentRouter = require("./routes/payment");
var transactionRouter = require("./routes/transaction");
var dashboardRouter = require("./routes/dashboard");
var penilaianRouter = require("./routes/penilaian");
var notifRouter = require("./routes/notif");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api", indexRouter);
app.use("/api/upload", uploadRouter);
app.use("/api/users", usersRouter);
app.use("/api/banner", bannerRouter);
app.use("/api/soal", soalRouter);
app.use("/api/attach", assetRouter);
app.use("/api/kelas", kelasRouter);
app.use("/api/voucher", voucherRouter);
app.use("/api/payment", paymentRouter);
app.use("/api/transaksi", transactionRouter);
app.use("/api/dashboard", dashboardRouter);
app.use("/api/penilaian", penilaianRouter);
app.use("/api/notif", notifRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
