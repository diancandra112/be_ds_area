"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_product.hasMany(models.ds_category_in_product, {
        foreignKey: "product_id",
      });
      ds_product.hasMany(models.ds_product_category, {
        foreignKey: "product_id",
      });
      ds_product.hasMany(models.ds_user_transaction_product, {
        foreignKey: "product_id",
      });
    }
  }
  ds_product.init(
    {
      having_expired: DataTypes.BOOLEAN,
      start_date: DataTypes.STRING,
      expired_date: DataTypes.STRING,
      nama_product: DataTypes.STRING,
      harga: DataTypes.DOUBLE,
      desc: DataTypes.TEXT,
      benefit: DataTypes.TEXT,
      image: DataTypes.TEXT,
      is_publish: DataTypes.BOOLEAN,
      publish_date: DataTypes.STRING,
      total_peserta: DataTypes.STRING,
      total_sub: DataTypes.STRING,
      total_soal: DataTypes.STRING,
      total_durasi: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "ds_product",
    }
  );
  return ds_product;
};
