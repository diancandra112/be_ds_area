'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ds_temp_file extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ds_temp_file.init({
    path: DataTypes.STRING,
    time: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ds_temp_file',
  });
  return ds_temp_file;
};