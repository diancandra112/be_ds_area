"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_sub_category_in_product_siswa extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ds_sub_category_in_product_siswa.init(
    {
      user_id: DataTypes.INTEGER,
      sub_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      category_id: DataTypes.INTEGER,
      score: DataTypes.INTEGER,
      presentase_penilaian: DataTypes.DOUBLE,
      now_duration: DataTypes.STRING,
      end_duration: DataTypes.STRING,
      tanggal_pengerjaan: DataTypes.STRING,
      jumlah_akses: DataTypes.INTEGER,
      is_done: DataTypes.BOOLEAN,

      tanggal_submit_pengerjaan: DataTypes.STRING,
      durasi_pengerjaan: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "ds_sub_category_in_product_siswa",
    }
  );
  return ds_sub_category_in_product_siswa;
};
