"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_user_answer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ds_user_answer.init(
    {
      product_id: DataTypes.INTEGER,
      sub_id: DataTypes.INTEGER,
      soal_id: DataTypes.INTEGER,
      jawaban_id: DataTypes.INTEGER,
      key: DataTypes.STRING,
      jawaban: DataTypes.TEXT,
      nilai: DataTypes.DOUBLE,
      is_penilaian: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "ds_user_answer",
    }
  );
  return ds_user_answer;
};
