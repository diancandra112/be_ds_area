"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_sub_category_in_product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_sub_category_in_product.belongsTo(models.ds_category_in_product, {
        foreignKey: "category_id",
      });
      ds_sub_category_in_product.hasMany(
        models.ds_sub_category_soal_in_product,
        { foreignKey: "sub_id", as: "soal" }
      );
    }
  }
  ds_sub_category_in_product.init(
    {
      title: DataTypes.STRING,
      product_id: DataTypes.INTEGER,
      category_id: DataTypes.INTEGER,
      duration: DataTypes.INTEGER,
      rules: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "ds_sub_category_in_product",
    }
  );
  return ds_sub_category_in_product;
};
