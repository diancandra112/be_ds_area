"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_sub_category_soal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_sub_category_soal.belongsTo(models.ds_sub_category, {
        foreignKey: "sub_id",
      });
      ds_sub_category_soal.hasMany(models.ds_sub_category_jawaban, {
        foreignKey: "soal_id",
      });
      ds_sub_category_soal.hasMany(models.ds_sub_category_jawaban, {
        foreignKey: "soal_id",
        as: "jawaban",
      });
    }
  }
  ds_sub_category_soal.init(
    {
      sub_id: DataTypes.INTEGER,
      no: DataTypes.INTEGER,
      soal: DataTypes.TEXT,
      type: DataTypes.STRING,
      image: DataTypes.TEXT,
      audio: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "ds_sub_category_soal",
    }
  );
  return ds_sub_category_soal;
};
