"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_user_jawaban extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_user_jawaban.belongsTo(models.ds_sub_category_soal_in_product, {
        foreignKey: "soal_id",
      });
    }
  }
  ds_user_jawaban.init(
    {
      user_id: DataTypes.INTEGER,
      jawaban_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      sub_id: DataTypes.INTEGER,
      soal_id: DataTypes.INTEGER,
      key: DataTypes.STRING,
      jawaban: DataTypes.STRING,
      image: DataTypes.TEXT,
      audio: DataTypes.TEXT,
      nilai: DataTypes.INTEGER,
      is_koreksi: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "ds_user_jawaban",
    }
  );
  return ds_user_jawaban;
};
