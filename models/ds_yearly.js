'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ds_yearly extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ds_yearly.init({
    year: DataTypes.STRING,
    net: DataTypes.DOUBLE,
    gross: DataTypes.DOUBLE,
    pending: DataTypes.DOUBLE,
    failed: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'ds_yearly',
  });
  return ds_yearly;
};