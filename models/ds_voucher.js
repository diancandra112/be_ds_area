"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_voucher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_voucher.hasMany(models.ds_user_transaction_product, {
        foreignKey: "voucher_id",
      });
    }
  }
  ds_voucher.init(
    {
      name: DataTypes.STRING,
      code: DataTypes.STRING,
      kuota: DataTypes.INTEGER,
      sisa_kuota: DataTypes.INTEGER,
      expired_at: DataTypes.STRING,
      diskon: DataTypes.INTEGER,
      is_unlimited: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "ds_voucher",
    }
  );
  return ds_voucher;
};
