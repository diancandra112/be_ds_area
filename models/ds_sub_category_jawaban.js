"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_sub_category_jawaban extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_sub_category_jawaban.belongsTo(models.ds_sub_category_soal, {
        foreignKey: "soal_id",
      });
    }
  }
  ds_sub_category_jawaban.init(
    {
      sub_id: DataTypes.INTEGER,
      soal_id: DataTypes.INTEGER,
      key: DataTypes.STRING,
      jawaban: DataTypes.TEXT,
      nilai: DataTypes.DOUBLE,
      image: DataTypes.TEXT,
      audio: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "ds_sub_category_jawaban",
    }
  );
  return ds_sub_category_jawaban;
};
