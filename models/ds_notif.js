"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_notif extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ds_notif.init(
    {
      title: DataTypes.STRING,
      body: DataTypes.STRING,
      tanggal: DataTypes.STRING,
      product_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      trx_id: DataTypes.INTEGER,
      is_read: DataTypes.BOOLEAN,
      link: { type: DataTypes.TEXT, defaultValue: "" },
    },
    {
      sequelize,
      modelName: "ds_notif",
    }
  );
  return ds_notif;
};
