"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_category_in_product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_category_in_product.belongsTo(models.ds_product, {
        foreignKey: "product_id",
      });

      ds_category_in_product.hasMany(models.ds_sub_category_in_product, {
        foreignKey: "category_id",
      });
    }
  }
  ds_category_in_product.init(
    {
      product_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      desc: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "ds_category_in_product",
    }
  );
  return ds_category_in_product;
};
