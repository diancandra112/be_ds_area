"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_category.hasMany(models.ds_product_category, {
        foreignKey: "category_id",
      });

      ds_category.hasMany(models.ds_sub_category, {
        foreignKey: "category_id",
      });
    }
  }
  ds_category.init(
    {
      name: DataTypes.STRING,
      desc: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "ds_category",
    }
  );
  return ds_category;
};
