"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_sub_category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_sub_category.hasMany(models.ds_sub_category_soal, {
        foreignKey: "sub_id",
      });
      ds_sub_category.hasMany(models.ds_sub_category_soal, {
        foreignKey: "sub_id",
        as: "soal",
      });

      ds_sub_category.belongsTo(models.ds_category, {
        foreignKey: "category_id",
      });
    }
  }
  ds_sub_category.init(
    {
      title: DataTypes.STRING,
      category_id: DataTypes.INTEGER,
      duration: DataTypes.INTEGER,
      rules: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "ds_sub_category",
    }
  );
  return ds_sub_category;
};
