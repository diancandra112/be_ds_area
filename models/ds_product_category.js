"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_product_category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_product_category.belongsTo(models.ds_product, {
        foreignKey: "product_id",
      });
      ds_product_category.belongsTo(models.ds_category, {
        foreignKey: "category_id",
      });
    }
  }
  ds_product_category.init(
    {
      product_id: DataTypes.INTEGER,
      category_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "ds_product_category",
    }
  );
  return ds_product_category;
};
