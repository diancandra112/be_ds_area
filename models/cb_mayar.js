'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cb_mayar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cb_mayar.init({
    data: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'cb_mayar',
  });
  return cb_mayar;
};