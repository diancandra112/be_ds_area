"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_banner extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ds_banner.init(
    {
      title: DataTypes.STRING,
      desc: DataTypes.TEXT,
      image: DataTypes.TEXT,
      link: DataTypes.TEXT,
      status: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "ds_banner",
    }
  );
  return ds_banner;
};
