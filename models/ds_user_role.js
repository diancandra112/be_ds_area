"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_user_role extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_user_role.hasMany(models.ds_user, { foreignKey: "role_id" });
    }
  }
  ds_user_role.init(
    {
      name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "ds_user_role",
    }
  );
  return ds_user_role;
};
