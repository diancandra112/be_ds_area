"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_user.belongsTo(models.ds_user_role, { foreignKey: "role_id" });
      ds_user.hasMany(models.ds_user_transaction_product, {
        foreignKey: "user_id",
      });
    }
  }
  ds_user.init(
    {
      user_id: DataTypes.STRING,
      email: DataTypes.STRING,
      verified_email: DataTypes.BOOLEAN,
      name: DataTypes.STRING,
      given_name: DataTypes.STRING,
      picture: DataTypes.TEXT,
      role_id: DataTypes.INTEGER,
      status: DataTypes.BOOLEAN,
      last_access: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "ds_user",
    }
  );
  return ds_user;
};
