"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_user_transaction_product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_user_transaction_product.belongsTo(models.ds_user, {
        foreignKey: "user_id",
      });
      ds_user_transaction_product.belongsTo(models.ds_product, {
        foreignKey: "product_id",
      });
      ds_user_transaction_product.belongsTo(models.ds_voucher, {
        foreignKey: "voucher_id",
      });
    }
  }
  ds_user_transaction_product.init(
    {
      invoice: DataTypes.STRING,
      user_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      status: DataTypes.ENUM(["success", "pending", "failed"]),
      mayar_id: DataTypes.STRING,
      mayar_transaction_id: DataTypes.STRING,
      mayar_link: DataTypes.STRING,
      payload_mayar: DataTypes.TEXT,

      voucher_id: DataTypes.INTEGER,
      voucher: DataTypes.STRING,
      discount: DataTypes.DOUBLE,
      harga: DataTypes.DOUBLE,
      total_pembayaran: DataTypes.DOUBLE,

      tanggal_transaksi: DataTypes.STRING,
      tahun_transaksi: DataTypes.STRING,
      periode_transaksi: DataTypes.STRING,
      tanggal_pembayaran: DataTypes.STRING,

      score: DataTypes.DOUBLE,
      presentase_penilaian: DataTypes.DOUBLE,

      durasi_pengerjaan: DataTypes.DOUBLE,

      rank: DataTypes.DOUBLE,
    },
    {
      sequelize,
      modelName: "ds_user_transaction_product",
    }
  );
  return ds_user_transaction_product;
};
