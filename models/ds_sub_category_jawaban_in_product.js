"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ds_sub_category_jawaban_in_product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ds_sub_category_jawaban_in_product.belongsTo(
        models.ds_sub_category_soal_in_product,
        { foreignKey: "soal_id" }
      );
    }
  }
  ds_sub_category_jawaban_in_product.init(
    {
      product_id: DataTypes.INTEGER,
      sub_id: DataTypes.INTEGER,
      soal_id: DataTypes.INTEGER,
      key: DataTypes.STRING,
      jawaban: DataTypes.TEXT,
      nilai: DataTypes.DOUBLE,
      image: DataTypes.TEXT,
      audio: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "ds_sub_category_jawaban_in_product",
    }
  );
  return ds_sub_category_jawaban_in_product;
};
