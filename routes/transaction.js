var express = require("express");
const { AuthAll } = require("../middlewares/jwt_auth");

var multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const { CtrlListTrx } = require("../controllers/CtrlTransaction");
var router = express.Router();

/* GET users listing. */
router.get("/list", AuthAll, CtrlListTrx);

module.exports = router;
