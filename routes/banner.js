var express = require("express");
const { AuthSADanAdmin, AuthAll } = require("../middlewares/jwt_auth");

var multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const {
  CtrlListBanner,
  CtrlCreteBanner,
  CtrlChangeBanner,
  CtrlDeleteBenner,
} = require("../controllers/CtrlBanner");
var router = express.Router();

/* GET users listing. */
router
  .get("/list", AuthAll, CtrlListBanner)
  .post("/create", AuthSADanAdmin, upload.single("image"), CtrlCreteBanner)
  .put("/change/status", AuthSADanAdmin, CtrlChangeBanner)
  .delete("/delete/:id", AuthSADanAdmin, CtrlDeleteBenner);

module.exports = router;
