var express = require("express");
var router = express.Router();
const { google } = require("googleapis");
const { sign } = require("jsonwebtoken");
const models = require("../models/index");
let url_be = "https://api-dsarea.aitilokal.com";
let url_fe = "https://dsarea-tryout.vercel.app";
require("dotenv").config();
const oauth2Client = new google.auth.OAuth2(
  process.env.client_id,
  process.env.client_secret,
  `${url_be}/api/auth/google/callback`
);
const scopes = [
  "https://www.googleapis.com/auth/userinfo.email",
  "https://www.googleapis.com/auth/userinfo.profile",
];

const authorizationUrl = oauth2Client.generateAuthUrl({
  access_type: "offline",
  scope: scopes,
  include_granted_scopes: true,
});
/* GET home page. */
router
  .get("/auth/google", function (req, res, next) {
    res.redirect(authorizationUrl);
  })
  .get("/auth/google/callback", async function (req, res, next) {
    try {
      const { code } = req.query;
      const { tokens } = await oauth2Client.getToken(code);
      oauth2Client.setCredentials(tokens);
      const oauth2 = google.oauth2({ auth: oauth2Client, version: "v2" });
      const { data } = await oauth2.userinfo.get();
      if (!data) {
        return res.json({ data: data });
      }
      let [user, created] = await models.ds_user.findOrCreate({
        where: { email: data.email },
        defaults: {
          user_id: data.id,
          email: data.email,
          verified_email: data.verified_email,
          name: data.name,
          given_name: data.given_name,
          picture: data.picture,
          role_id: 3,
        },
      });
      user = JSON.parse(JSON.stringify(user));
      let payload = {
        id: user.id,
        role_id: user.role_id,
        user_id: user.user_id,
        email: user.email,
        name: user.name,
      };
      const secret = process.env.jwt_secret;
      const token = sign(payload, secret, { expiresIn: "360 days" });
      res.redirect(
        `${url_fe}/auth-success?token=${token}&role_id=${payload.role_id}`
      );
      // return res.json({ data, token });
    } catch (error) {
      console.log(error);
      return res
        .status(404)
        .json({ responseCode: 404, message: error.message });
    }
  });

module.exports = router;
