var express = require("express");
var router = express.Router();
const models = require("../models/index");
const moment = require("moment");
const { Op } = require("sequelize");
/* GET users listing. */
router.all("/callback", async (req, res) => {
  try {
    const data = req.body;
    await models.cb_mayar.create({
      data: JSON.stringify(data),
    });
    if (data.event == "payment.received") {
      let id = data.data.productId;
      let trx = await models.ds_user_transaction_product.findOne({
        where: { mayar_id: id },
      });
      if (trx == null) {
        throw new Error("Product Not Found");
      } else if (trx.status == "success") {
        return res.json({
          responseCode: 200,
          message: "Product Telah Dibayar",
        });
      }
      let status = "failed";
      if (data.data.status == "SUCCESS") {
        status = "success";
      }
      await models.ds_user_transaction_product.update(
        {
          status: status,
          payload_mayar: JSON.stringify(data),
          tanggal_pembayaran: moment().format("YYYY-MM-DD HH:mm:ss"),
        },
        {
          where: { mayar_id: id },
        }
      );

      await models.ds_notif.bulkCreate([
        {
          title: "Pembayaran Berhasil",
          body: `Pembelian Product Dengan Invoice ${trx.invoice} Berhasil Dilakukan`,
          tanggal: moment().format("YYYY-MM-DD HH:mm:ss"),
          product_id: trx.product_id,
          user_id: trx.user_id,
          trx_id: trx.id,
        },
        {
          title: "Pembayaran Berhasil",
          body: `Pembelian Product Dengan Invoice ${trx.invoice} Berhasil Dilakukan`,
          tanggal: moment().format("YYYY-MM-DD HH:mm:ss"),
          product_id: trx.product_id,
          user_id: 0,
          trx_id: trx.id,
        },
      ]);
    }
    return res.json({ responseCode: 200, message: "ok" });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ responseCode: 500, message: error.message });
  }
});

module.exports = router;
