var express = require("express");
const { AuthAll } = require("../middlewares/jwt_auth");
const models = require("../models/index");
var router = express.Router();

/* GET users listing. */
router
  .get("/list", AuthAll, async (req, res) => {
    try {
      const data = req.decoded;
      let user_id = 0;
      if (data.role_id == 3) {
        user_id = 1;
      }
      await models.ds_notif.update(
        { is_read: true },
        {
          where: { user_id: user_id, is_read: false },
        }
      );
      let notif = await models.ds_notif.findAll({
        where: { user_id: user_id },
        order: [["id", "DESC"]],
        limit: 100,
      });
      res.status(200).json({
        responseCode: 200,
        message: "Success Publish Data",
        data: notif,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  })
  .get("/jumlah/unread", AuthAll, async (req, res) => {
    try {
      const data = req.decoded;
      let user_id = 0;
      if (data.role_id == 3) {
        user_id = 1;
      }
      let notif = await models.ds_notif.count({
        where: { user_id: user_id, is_read: false },
        order: [["id", "DESC"]],
      });
      res.status(200).json({
        responseCode: 200,
        message: "Success Publish Data",
        data: notif,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  });

module.exports = router;
