var express = require("express");
const { AuthSADanAdmin, AuthAll } = require("../middlewares/jwt_auth");
var multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
var router = express.Router();
const moment = require("moment");
const models = require("../models/index");
const fs = require("fs");
var path = require("path");
var sharp = require("sharp");
/* GET users listing. */
router.post(
  "/file",
  AuthSADanAdmin,
  upload.single("file"),
  async (req, res) => {
    try {
      if (req.file == null) {
        throw new Error("GOBLOK OJOK DI KOSONGIN");
      }
      let dir = "upload_" + moment().format("YYYY-MM");
      const element = req.file;
      fs.access(`./${dir}/`, (error) => {
        if (error) {
          fs.mkdirSync(`./${dir}/`);
        }
      });
      let uniqueSuffix =
        (Date.now() + Math.round(Math.random() * 1e9)).toString() +
        element.originalname;
      try {
        await sharp(element.buffer)
          .webp({ quality: 10 })
          .toFile(path.resolve(`./${dir}`, uniqueSuffix));
      } catch (error) {
        console.log(error);
        await fs.writeFileSync(
          path.resolve(`./${dir}`, uniqueSuffix),
          element.buffer
        );
      }
      let db = {
        path: `./${dir}/${uniqueSuffix}`,
        time: moment().format("YYYY-MM-DD HH:mm:ss"),
      };
      await models.ds_temp_file.create(db);
      res.status(200).json({
        responseCode: 200,
        message: "Sukses Upload Data",
        data: Buffer.from(
          uniqueSuffix + ".-splash-." + element.mimetype + ".-splash-." + dir
        ).toString("base64url"),
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  }
);

module.exports = router;
