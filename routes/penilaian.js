var express = require("express");
const { AuthSADanAdmin, AuthAll } = require("../middlewares/jwt_auth");

const {
  CtrlListPenilaian,
  CtrlSoalPenilaian,
  CtrlSubmitPenilaian,
  CtrlDetailPenilaian,
  CtrlPublishPenilaian,
} = require("../controllers/CtrlPenilaian");
var router = express.Router();

/* GET users listing. */
router
  .get("/list", AuthSADanAdmin, CtrlListPenilaian)
  .get("/detail/:product_id", AuthSADanAdmin, CtrlDetailPenilaian)
  .post("/submit", AuthSADanAdmin, CtrlSubmitPenilaian)
  .post("/publish/:product_id", AuthSADanAdmin, CtrlPublishPenilaian)
  .get(
    "/list/soal/:product_id/:category_id/:sub_id/:user_id",
    AuthAll,
    CtrlSoalPenilaian
  );

module.exports = router;
