var express = require("express");
const { AuthSADanAdmin, AuthAll } = require("../middlewares/jwt_auth");

const {
  CtrlCreateVch,
  CtrlListVch,
  CtrlVchCheck,
  CtrlVchCheckUser,
  CtrlEditVch,
} = require("../controllers/CtrlVoucher");
var router = express.Router();

/* GET users listing. */
router
  .post("/create", AuthSADanAdmin, CtrlCreateVch)
  .patch("/edit/:id", AuthSADanAdmin, CtrlEditVch)
  .get("/list", AuthSADanAdmin, CtrlListVch)
  .get("/check/:code", AuthSADanAdmin, CtrlVchCheck)
  .get("/user/check/:code/:product_id", AuthAll, CtrlVchCheckUser)
  .post("/test", AuthSADanAdmin, (req, res) => {
    function base64UrlToBase64(base64Url) {
      // Pad the Base64 URL string with "=" until its length is a multiple of 4
      while (base64Url.length % 4) {
        base64Url += "=";
      }

      // Replace characters according to the Base64 decoding rules
      let base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");

      return base64;
    }
    let data =
      "eyJpZCI6NDYsImxhaW5fbGFpbiI6eyJNYXRlcmFpIjowLCJCaWF5YSBUZXRhcCI6MTUwMDAsIkJpYXlhIFBlbWVsaWhhcmFhbiI6ODUwMH0sInN1Yl90b3RhbCI6NDM1MTAwMCwiZ3JhbmR0b3RhbCI6NDM3NDUwMCwic3RhcnRfZGF0ZSI6IjIwMjMtMTEtMjRUMTU6MDA6MDArMDc6MDAiLCJlbmRfZGF0ZSI6IjIwMjQtMDEtMTBUMDA6MDA6MDArMDc6MDAiLCJzdGFydF9tZXRlciI6NjQxOSwiZW5kX21ldGVyIjo2NjQ4LCJtaW5pbXVtX2NoYXJnZV90b3RhbCI6MCwiYmlsbGluZ191c2FnZSI6MjI5LCJ1c2FnZSI6MjI5LCJwYXJhbWV0ZXJfMSI6IjUwMDAwIiwicGFyYW1ldGVyXzIiOiI1MDAwMCIsInBhcmFtZXRlcl8zIjoiNzUwMDAiLCJwYXJhbWV0ZXJfNCI6IjEwMDAwMCIsInByaWNlX3BhcmFtZXRlcl8xIjoxOTAwMCwicHJpY2VfcGFyYW1ldGVyXzIiOjE4NjUwLCJwcmljZV9wYXJhbWV0ZXJfMyI6MTgyNTAsInByaWNlX3BhcmFtZXRlcl80IjoxNzkwMCwiYW1vdW50X3BhcmFtZXRlcl8xIjo0MzUxMDAwLCJhbW91bnRfcGFyYW1ldGVyXzIiOjAsImFtb3VudF9wYXJhbWV0ZXJfMyI6MCwiYW1vdW50X3BhcmFtZXRlcl80IjowLCJ1c2FnZV9wYXJhbWV0ZXJfMSI6MjI5LCJ1c2FnZV9wYXJhbWV0ZXJfMiI6MCwidXNhZ2VfcGFyYW1ldGVyXzMiOjAsInVzYWdlX3BhcmFtZXRlcl80IjowLCJkdWVfZGF0ZSI6bnVsbCwiY3V0X2RhdGUiOiIyMDI0LTAxLTEwIDAwOjAwOjAwIiwiaW52b2ljZSI6IktCTi5LQk4gQ2FrdW5nLjYwLUlOVi0xMDAxMjQtNDJHIiwiaWRfcGVsYW5nZ2FuIjpudWxsLCJ0ZW5hbnRfbmFtZSI6IkhBTlNBRSBJTkRPTkVTSUEgVVRBTUEgMiIsImJpbGxpbmdfYWRkcmVzcyI6IkpsLiBJcmlhbiBCbG9rIEUgTm8uIDA4In0";
    data = base64UrlToBase64(data);
    let decodedString = Buffer.from(data, "base64").toString("utf-8");
    decodedString = JSON.parse(decodedString);
    return res.json(decodedString);
  });

module.exports = router;
