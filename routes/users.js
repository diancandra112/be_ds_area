var express = require("express");
const { AuthSADanAdmin, AuthAll } = require("../middlewares/jwt_auth");
const {
  CtrlListUserRole,
  CtrlChangeUserRole,
  CtrlListRole,
  CtrlProfile,
  CtrlDetailProductPembelian,
  CtrlDetailUserProduct,
  CtrlPaymentProduct,
  CtrlDsCalender,
} = require("../controllers/CtrlUserRole");
const {
  CtrlLatihanSoalDetail,
  CtrlSubLatihanSoalDetail,
  CtrlSubLatihanSoal,
  CtrlUserJawabanPilihan,
  CtrlUserJawabanEssay,
  CtrlSubLatihanSoalSelesai,
} = require("../controllers/CtrlLatihanSoal");
var router = express.Router();

/* GET users listing. */
router
  .get("/profile", AuthAll, CtrlProfile)
  .get("/list", AuthSADanAdmin, CtrlListUserRole)
  .get("/siswa/list/product/:id", AuthAll, CtrlDetailUserProduct)
  .get("/siswa/dashboard/calender", AuthAll, CtrlDsCalender)
  .get(
    "/siswa/detail/product/owned/:product_id",
    AuthAll,
    CtrlLatihanSoalDetail
  )
  .get(
    "/siswa/detail/sub/product/owned/:product_id/:category_id/:sub_id",
    AuthAll,
    CtrlSubLatihanSoalDetail
  )
  .get(
    "/siswa/soal/sub/product/owned/:product_id/:category_id/:sub_id",
    AuthAll,
    CtrlSubLatihanSoal
  )
  .post("/siswa/jawaban/done", AuthAll, CtrlSubLatihanSoalSelesai)
  .post("/siswa/jawab/soal/pilihan", AuthAll, CtrlUserJawabanPilihan)
  .post("/siswa/jawab/soal/essay", AuthAll, CtrlUserJawabanEssay)

  .get("/pembelian/detail/product/:id", AuthAll, CtrlDetailProductPembelian)
  .get("/list/role", AuthSADanAdmin, CtrlListRole)
  .put("/change/role", AuthSADanAdmin, CtrlChangeUserRole)

  .post("/pembelian/product", AuthAll, CtrlPaymentProduct);

module.exports = router;
