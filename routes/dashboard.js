var express = require("express");
const { AuthSADanAdmin, AuthAll } = require("../middlewares/jwt_auth");

const {
  CtrlOverview,
  CtrlTransaksi,
  CtrlListProdcutDs,
  CtrlListPemenang,
} = require("../controllers/CtrlDashboard");
var router = express.Router();

/* GET users listing. */
router
  .get("/overview", AuthAll, CtrlOverview)
  .get("/transaksi/:tahun", AuthAll, CtrlTransaksi)
  .get("/list/product", AuthAll, CtrlListProdcutDs)
  .get("/list/pemenang/:product_id", AuthAll, CtrlListPemenang);

module.exports = router;
