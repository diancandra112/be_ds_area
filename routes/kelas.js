const { default: axios } = require("axios");
var express = require("express");
const { AuthSADanAdmin, AuthAll } = require("../middlewares/jwt_auth");
var router = express.Router();
require("dotenv").config();
/* GET users listing. */
router.get("/:type/:page/:size", AuthAll, async function (req, res, next) {
  try {
    let { type, page, size } = req.params;
    let link = process.env.mayar_url_shop + "/bootcamp/";
    let url = `product/type/cohort_based?page=${page}&pageSize=${size}`;
    if (type == "online") {
      link = process.env.mayar_url_shop + "/course/";
      url = `product/type/course?page=${page}&pageSize=${size}`;
    } else if (type != "bootcamp") {
      throw new Error("Type Not Valid");
    }
    let kelas = await axios({
      headers: { Authorization: process.env.mayar_token },
      baseURL: process.env.mayar_baseurl,
      url: url,
    });
    kelas = kelas.data.data;
    kelas.map((val) => {
      val.link = link + val.link;
    });
    return res.status(200).json({
      responseCode: 200,
      message: "Sukses Get Data",
      data: kelas,
    });
  } catch (error) {
    console.log(error);
    return res.status(400).json({ responseCode: 400, message: error.message });
  }
});

module.exports = router;
