var express = require("express");
const { AuthSADanAdmin, AuthAll } = require("../middlewares/jwt_auth");
const {
  CtrlCreateCategory,
  CtrlEditCategory,
  CtrlListCategory,
} = require("../controllers/soal/CtrlCategory");

var multer = require("multer");
const {
  CtrlCreateSubCategory,
  CtrlListSubCategory,
  CtrlCreateSubCategoryFrom,
  CtrlDetailSubCategory,
  CtrlEditSubCategory,
} = require("../controllers/soal/CtrlSubCategory");
const {
  CtrlCreateProduct,
  CtrlListProduct,
  CtrlEditProduct,
} = require("../controllers/soal/CtrlProduct");
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
var router = express.Router();

/* GET users listing. */
router
  .get("/category/list", AuthSADanAdmin, CtrlListCategory)
  .post("/category/create", AuthSADanAdmin, CtrlCreateCategory)
  .patch("/category/change/status/:id", AuthSADanAdmin, CtrlEditCategory)

  .post("/sub/category/create", AuthSADanAdmin, CtrlCreateSubCategory)
  .patch("/sub/category/edit/:id", AuthSADanAdmin, CtrlEditSubCategory)
  .get("/sub/category/detail/:id", AuthSADanAdmin, CtrlDetailSubCategory)
  .post(
    "/sub/category/create/form",
    AuthSADanAdmin,
    upload.any(),
    CtrlCreateSubCategoryFrom
  )

  .get("/sub/category/list", AuthSADanAdmin, CtrlListSubCategory)

  .post(
    "/product/create",
    AuthSADanAdmin,
    upload.single("image"),
    CtrlCreateProduct
  )
  .patch(
    "/product/edit/:id",
    AuthSADanAdmin,
    upload.single("image"),
    CtrlEditProduct
  )
  .get("/product/list", AuthAll, CtrlListProduct);

module.exports = router;
