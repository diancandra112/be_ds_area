var express = require("express");
var router = express.Router();
const fs = require("fs");
/* GET users listing. */
router.get("/:data", function (req, res, next) {
  try {
    let data = req.params.data;
    data = Buffer.from(data, "base64url").toString();
    data = data.split(".-splash-.");
    let path = "asset";
    if (data[2]) {
      path = data[2];
    }
    fs.readFile(`./${path}/${data[0]}`, function (err, content) {
      if (err) {
        res.writeHead(400, { "Content-type": "text/html" });
        console.log(err);
        res.end("No such image");
      } else {
        res.writeHead(200, { "Content-type": data[1] });
        res.end(content);
      }
    });
  } catch (error) {
    console.log(error);
    res.writeHead(400, { "Content-type": "text/html" });
    res.end("No such image");
  }
});

module.exports = router;
