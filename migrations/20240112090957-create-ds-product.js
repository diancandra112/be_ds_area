"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_products", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      having_expired: {
        type: Sequelize.BOOLEAN,
      },
      expired_date: {
        type: Sequelize.STRING,
      },
      nama_product: {
        type: Sequelize.STRING,
      },
      harga: {
        type: Sequelize.DOUBLE,
      },
      desc: {
        type: Sequelize.TEXT,
      },
      benefit: {
        type: Sequelize.TEXT,
      },
      image: {
        type: Sequelize.TEXT,
      },
      is_publish: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0,
      },
      publish_date: {
        type: Sequelize.STRING,
      },
      total_peserta: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      total_sub: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      total_soal: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      total_durasi: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex("ds_products", ["nama_product"], {
      name: "index",
      unique: true,
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_products");
  },
};
