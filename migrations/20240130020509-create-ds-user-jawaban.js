"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_user_jawabans", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      user_id: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      jawaban_id: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      product_id: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      sub_id: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      soal_id: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      nilai: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      key: {
        type: Sequelize.STRING,
      },
      jawaban: {
        type: Sequelize.STRING,
      },
      image: {
        type: Sequelize.TEXT,
      },
      is_koreksi: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      audio: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "ds_user_jawabans",
      ["user_id", "jawaban_id", "product_id", "sub_id", "soal_id"],
      {
        name: "index",
        unique: true,
      }
    );
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_user_jawabans");
  },
};
