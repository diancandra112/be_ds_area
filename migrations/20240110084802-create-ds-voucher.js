"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_vouchers", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
      },
      code: {
        type: Sequelize.STRING,
      },
      kuota: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
      },
      sisa_kuota: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
      },
      is_unlimited: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      expired_at: {
        type: Sequelize.STRING,
      },
      diskon: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex("ds_vouchers", ["code"], {
      name: "index",
      unique: false,
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_vouchers");
  },
};
