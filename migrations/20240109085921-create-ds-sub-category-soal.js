"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_sub_category_soals", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      sub_id: {
        type: Sequelize.INTEGER,
      },
      no: {
        type: Sequelize.INTEGER,
      },
      soal: {
        type: Sequelize.TEXT,
      },
      type: {
        type: Sequelize.STRING,
      },
      image: {
        type: Sequelize.TEXT,
        defaultValue: "-",
      },
      audio: {
        type: Sequelize.TEXT,
        defaultValue: "-",
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex("ds_sub_category_soals", ["sub_id"], {
      name: "index",
      unique: false,
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_sub_category_soals");
  },
};
