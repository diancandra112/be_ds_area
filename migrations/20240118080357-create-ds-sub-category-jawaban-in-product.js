"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_sub_category_jawaban_in_products", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      product_id: {
        type: Sequelize.INTEGER,
      },
      sub_id: {
        type: Sequelize.INTEGER,
      },
      soal_id: {
        type: Sequelize.INTEGER,
      },
      key: {
        type: Sequelize.STRING,
      },
      jawaban: {
        type: Sequelize.TEXT,
      },
      nilai: {
        type: Sequelize.DOUBLE,
      },
      image: {
        type: Sequelize.TEXT,
      },
      audio: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "ds_sub_category_jawaban_in_products",
      ["sub_id", "soal_id", "key", "product_id"],
      {
        name: "index",
        unique: false,
      }
    );
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_sub_category_jawaban_in_products");
  },
};
