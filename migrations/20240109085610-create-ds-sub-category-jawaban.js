"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_sub_category_jawabans", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      sub_id: {
        type: Sequelize.INTEGER,
      },
      soal_id: {
        type: Sequelize.INTEGER,
      },
      key: {
        type: Sequelize.STRING,
      },
      jawaban: {
        type: Sequelize.TEXT,
      },
      nilai: {
        type: Sequelize.DOUBLE,
      },
      image: {
        type: Sequelize.TEXT,
        defaultValue: "-",
      },
      audio: {
        type: Sequelize.TEXT,
        defaultValue: "-",
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "ds_sub_category_jawabans",
      ["sub_id", "soal_id", "key"],
      {
        name: "index",
        unique: false,
      }
    );
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_sub_category_jawabans");
  },
};
