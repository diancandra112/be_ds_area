"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_user_transaction_products", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      invoice: {
        type: Sequelize.STRING,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      product_id: {
        type: Sequelize.INTEGER,
      },
      status: {
        type: Sequelize.ENUM(["success", "pending", "failed"]),
      },
      mayar_id: {
        type: Sequelize.STRING,
      },
      mayar_transaction_id: {
        type: Sequelize.STRING,
      },
      mayar_link: {
        type: Sequelize.STRING,
      },
      payload_mayar: {
        type: Sequelize.TEXT,
      },
      rank: {
        type: DataTypes.DOUBLE,
        defaultValue: 0,
      },
      score: {
        type: DataTypes.DOUBLE,
        defaultValue: 0,
      },
      durasi_pengerjaan: {
        type: DataTypes.DOUBLE,
        defaultValue: 0,
      },
      presentase_penilaian: {
        type: DataTypes.DOUBLE,
        defaultValue: 0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "ds_user_transaction_products",
      ["mayar_id", "mayar_transaction_id", "status"],
      {
        name: "index",
        unique: false,
      }
    );
    await queryInterface.addIndex("ds_user_transaction_products", ["invoice"], {
      name: "index2",
      unique: true,
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_user_transaction_products");
  },
};
