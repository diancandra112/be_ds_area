"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "ds_user_transaction_products",
          "voucher_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "ds_user_transaction_products",
          "voucher",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "ds_user_transaction_products",
          "discount",
          {
            type: Sequelize.DOUBLE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "ds_user_transaction_products",
          "harga",
          {
            type: Sequelize.DOUBLE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "ds_user_transaction_products",
          "total_pembayaran",
          {
            type: Sequelize.DOUBLE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "ds_user_transaction_products",
          "tanggal_pembayaran",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn(
          "ds_user_transaction_products",
          "voucher_id",
          {
            transaction: t,
          }
        ),
        queryInterface.removeColumn("ds_user_transaction_products", "voucher", {
          transaction: t,
        }),
        queryInterface.removeColumn(
          "ds_user_transaction_products",
          "discount",
          {
            transaction: t,
          }
        ),
        queryInterface.removeColumn("ds_user_transaction_products", "harga", {
          transaction: t,
        }),
        queryInterface.removeColumn(
          "ds_user_transaction_products",
          "total_pembayaran",
          {
            transaction: t,
          }
        ),
        queryInterface.removeColumn(
          "ds_user_transaction_products",
          "tanggal_pembayaran",
          {
            transaction: t,
          }
        ),
      ]);
    });
  },
};
