"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_sub_category_in_product_siswas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      sub_id: {
        type: Sequelize.INTEGER,
      },
      product_id: {
        type: Sequelize.INTEGER,
      },
      category_id: {
        type: Sequelize.INTEGER,
      },
      score: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      presentase_penilaian: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      durasi_pengerjaan: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      now_duration: {
        type: Sequelize.STRING,
      },
      end_duration: {
        type: Sequelize.STRING,
      },
      tanggal_pengerjaan: {
        type: Sequelize.STRING,
      },
      tanggal_submit_pengerjaan: {
        type: Sequelize.STRING,
      },
      jumlah_akses: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      is_done: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_sub_category_in_product_siswas");
  },
};
