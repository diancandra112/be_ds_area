"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("ds_sub_category_soal_in_products", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      product_id: {
        type: Sequelize.INTEGER,
      },
      sub_id: {
        type: Sequelize.INTEGER,
      },
      no: {
        type: Sequelize.INTEGER,
      },
      soal: {
        type: Sequelize.TEXT,
      },
      type: {
        type: Sequelize.STRING,
      },
      image: {
        type: Sequelize.TEXT,
      },
      audio: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "ds_sub_category_soal_in_products",
      ["sub_id", "product_id"],
      {
        name: "index",
        unique: false,
      }
    );
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("ds_sub_category_soal_in_products");
  },
};
