const { Op } = require("sequelize");
const models = require("../models/index");

const fs = require("fs");
var path = require("path");
var sharp = require("sharp");
module.exports = {
  CtrlListBanner: async (req, res) => {
    try {
      const data = req.query;
      let banner = await models.ds_banner.findAll({
        where: data,
        order: [["id", "DESC"]],
      });
      banner = JSON.parse(JSON.stringify(banner));
      var urlimg = req.protocol + "://" + req.get("host") + `/api/attach/`;
      banner.map((val) => {
        val.image = urlimg + val.image;
      });
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: banner });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlChangeBanner: async (req, res) => {
    try {
      const data = req.body;
      let update = {};
      let flag = ["status", "link"];
      flag.map((val) => {
        if (data[val] != null) {
          update[val] = data[val];
        }
      });
      let user = await models.ds_banner.findOne({
        where: { id: data.id },
        attributes: ["id"],
      });
      if (user == null) {
        throw new Error("User Not Found");
      }
      await models.ds_banner.update(update, {
        where: { id: data.id },
      });
      res.status(200).json({ responseCode: 200, message: "Success Edit Data" });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlDeleteBenner: async (req, res) => {
    try {
      const data = req.params;
      let user = await models.ds_banner.findOne({
        where: { id: data.id },
      });
      user = JSON.parse(JSON.stringify(user));
      if (user == null) {
        throw new Error("User Not Found");
      }
      await models.ds_banner.destroy({
        where: { id: data.id },
      });
      try {
        let img = Buffer.from(user.image, "base64url").toString();
        img = img.split(".-splash-.");
        await fs.unlinkSync(`./asset/${img[0]}`);
      } catch (error) {
        console.log(error);
      }
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Delete Data" });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlCreteBanner: async (req, res) => {
    try {
      const data = req.body;
      if (req.file) {
        try {
          fs.access("./asset/", (error) => {
            if (error) {
              fs.mkdirSync("./asset/");
            }
          });
          let uniqueSuffix =
            (Date.now() + Math.round(Math.random() * 1e9)).toString() +
            req.file.originalname;
          await sharp(req.file.buffer)
            .webp({ quality: 10 })
            .toFile(path.resolve("./asset", uniqueSuffix));
          data.image = Buffer.from(
            uniqueSuffix + ".-splash-." + req.file.mimetype
          ).toString("base64url");
        } catch (error) {
          console.log(error);
        }
      }
      await models.ds_banner.create(data);
      res.status(200).json({
        responseCode: 200,
        message: "Success Create Data",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
