const { Op } = require("sequelize");
const moment = require("moment");
const models = require("../models/index");
const { default: axios } = require("axios");
require("dotenv").config();
function groupDataById(data) {
  const groupedData = {};

  data.forEach((item) => {
    const itemId = item.id;

    if (!groupedData[itemId]) {
      groupedData[itemId] = {
        id: itemId,
        image: item.image,
        expired_at: item.expired_at,
        nama_product: item.nama_product,
        harga: item.harga,
        category_name: [item.category_name],
        total_duration: item.total_duration,
        score: item.score,
        benefit: item.benefit,
        desc: item.desc,
        total_pembelian: item.total_pembelian,
        participant: item.participant,
        rank: item.rank,
        total_soal: item.total_soal,
        total_jawab: item.total_jawab,
        status: item.status,
        start_date: item.start_date,
        end_date: item.end_date,
      };
    } else {
      groupedData[itemId].category_name.push(item.category_name);
    }
  });

  const result = Object.values(groupedData);
  return result;
}
function addLeadingZeros(input) {
  const zerosToAdd = 5;
  const inputString = String(input);

  if (inputString.length >= zerosToAdd) {
    return inputString;
  } else {
    const numberOfZeros = zerosToAdd - inputString.length;
    const zeros = "0".repeat(numberOfZeros);
    return zeros + inputString;
  }
}
module.exports = {
  CtrlLatihanSoalDetail: async (req, res) => {
    try {
      let { id } = req.decoded;
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      let { product_id } = req.params;
      let user = await models.sequelize.query(`SELECT
      b.id,
      b.benefit,
      b.desc,
      b.expired_date AS expired_at,
      b.nama_product,
      b.harga,
      a.rank,
      (SELECT COUNT(cc.id) FROM ds_user_jawabans AS cc WHERE cc.product_id = b.id AND user_id=${id}) as total_jawab, 
      (SELECT COUNT(cc.id) FROM ds_user_transaction_products AS cc WHERE cc.product_id = b.id) as participant, 
      c.NAME AS category_name,
      ( SELECT SUM( d.duration ) FROM ds_sub_category_in_products AS d WHERE b.id = d.product_id ) AS total_duration,
      ( SELECT COUNT( e.id ) FROM ds_sub_category_soal_in_products AS e WHERE b.id = e.product_id ) AS total_soal,
      a.score
    FROM
    ds_user_transaction_products AS a
      INNER JOIN ds_products AS b ON a.product_id = b.id
      INNER JOIN ds_category_in_products AS c ON b.id = c.product_id 
    WHERE
      a.user_id =${id} AND a.product_id =${product_id}`);
      user = user[0];
      if (user.length == 0) {
        throw new Error("Not Found");
      }
      user = groupDataById(user);
      let sub = await models.sequelize.query(`
      SELECT a.category_id,
      a.product_id, 
      a.id AS sub_id, 
      a.title, 
      b.name AS 'category_name', 
      duration, 
      IFNULL(e.score,0) AS score,
      e.tanggal_pengerjaan,
      IFNULL(e.jumlah_akses,0) AS jumlah_akses,
	    CASE 
		    WHEN e.is_done = 1 THEN 'done'
		    WHEN c.expired_date < '${now}' THEN 'expired'
		    WHEN e.jumlah_akses > 5 THEN 'pinalty'
		    ELSE 'active'
	    END AS 'status'
      FROM ds_sub_category_in_products AS a
      INNER JOIN ds_category_in_products AS b ON a.category_id = b.id 
      INNER JOIN ds_products AS c ON a.product_id=c.id
	    LEFT JOIN ds_sub_category_in_product_siswas AS e ON a.product_id = e.product_id 
      AND a.id=e.sub_id AND a.category_id=e.category_id AND e.user_id=${id} 
      WHERE a.product_id=${product_id}`);
      sub = sub[0];
      user = user[0];
      user.sub_category = sub;
      user.total_dilewati = user.total_soal - user.total_jawab;
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: user });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlSubLatihanSoalDetail: async (req, res) => {
    try {
      let { id } = req.decoded;
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      let { product_id, category_id, sub_id } = req.params;
      let user = await models.sequelize.query(`
        SELECT a.id AS product_id,
        f.id AS category_id,
        b.id AS sub_id, 
        a.nama_product, 
        a.expired_date,
      CASE 
        WHEN e.is_done = 1 THEN 'done'
        WHEN a.expired_date < '${now}' THEN 'expired'
		    WHEN e.jumlah_akses > 5 THEN 'pinalty'
        ELSE 'active'
      END AS status,
      b.duration, 
      COUNT(c.id) AS total_soal,  
      (SELECT COUNT(jw.id) FROM ds_user_jawabans AS jw WHERE jw.sub_id = b.id AND jw.user_id=${id}) as total_soal_answer, 
      (SELECT COUNT(cc.id) FROM ds_user_transaction_products AS cc WHERE cc.product_id = a.id) as participant, 
      f.name AS category_name,
      b.title AS sub_category_name, 
      b.rules FROM ds_products AS a
      INNER JOIN ds_category_in_products AS f ON a.id = f.product_id
      INNER JOIN ds_sub_category_in_products AS b ON a.id = b.product_id AND b.category_id = f.id
      LEFT JOIN ds_sub_category_soal_in_products AS c ON b.id = c.sub_id
	    LEFT JOIN ds_sub_category_in_product_siswas AS e ON  e.product_id = ${product_id}
      AND e.sub_id=${sub_id} AND e.category_id=${category_id} AND e.user_id=${id} 
      WHERE a.id = ${product_id} AND b.category_id =${category_id} AND b.id = ${sub_id}
      GROUP BY a.nama_product, a.expired_date, status, b.duration, f.name, b.title,b.rules`);
      user = user[0];
      user.map((val) => {
        val.total_dilewati = val.total_soal - val.total_soal_answer;
      });
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: user });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlSubLatihanSoal: async (req, res) => {
    try {
      let { id } = req.decoded;
      let { product_id, category_id, sub_id } = req.params;
      var urlimg = req.protocol + "://" + req.get("host") + `/api/attach/`;
      let trx = await models.ds_user_transaction_product.findOne({
        where: { user_id: id, product_id: product_id, status: "success" },
      });
      if (trx == null) {
        throw new Error("Bisa Beli Dulu DI Product");
      }
      let sub_cat_siswa = await models.ds_sub_category_in_product_siswa.findOne(
        {
          where: {
            user_id: id,
            sub_id: sub_id,
            product_id: product_id,
            category_id: category_id,
          },
        }
      );
      let sub_cat = await models.ds_sub_category_in_product.findOne({
        where: {
          id: sub_id,
          product_id: product_id,
          category_id: category_id,
        },
        include: [
          {
            model: models.ds_sub_category_soal_in_product,
            as: "soal",
            attributes: [
              ["id", "soal_id"],
              "product_id",
              "sub_id",
              "no",
              "soal",
              "type",
              [
                models.Sequelize.fn(
                  "CONCAT",
                  models.Sequelize.literal(`'${urlimg}'`),
                  models.Sequelize.col("soal.image")
                ),
                "image",
              ],
              [
                models.Sequelize.fn(
                  "CONCAT",
                  models.Sequelize.literal(`'${urlimg}'`),
                  models.Sequelize.col("soal.audio")
                ),
                "audio",
              ],
            ],
            include: [
              {
                required: false,
                model: models.ds_sub_category_jawaban_in_product,
                attributes: [
                  ["id", "jawaban_id"],
                  "product_id",
                  "sub_id",
                  "soal_id",
                  "key",
                  "jawaban",
                  [
                    models.Sequelize.fn(
                      "CONCAT",
                      models.Sequelize.literal(`'${urlimg}'`),
                      models.Sequelize.col("soal.jawaban.image")
                    ),
                    "image",
                  ],
                  [
                    models.Sequelize.fn(
                      "CONCAT",
                      models.Sequelize.literal(`'${urlimg}'`),
                      models.Sequelize.col("soal.jawaban.audio")
                    ),
                    "audio",
                  ],
                ],
                as: "jawaban",
              },
              {
                required: false,
                model: models.ds_user_jawaban,
                as: "jawaban_user",
                where: { user_id: id },
              },
            ],
          },
        ],
      });
      sub_cat = JSON.parse(JSON.stringify(sub_cat));

      let now_duration = moment().format("YYYY-MM-DD HH:mm:ss");
      let end_duration = moment(now_duration)
        .add(sub_cat.duration, "minute")
        .format("YYYY-MM-DD HH:mm:ss");
      if (sub_cat_siswa == null) {
        sub_cat_siswa = await models.ds_sub_category_in_product_siswa.create({
          user_id: id,
          sub_id: sub_id,
          product_id: product_id,
          category_id: category_id,
          score: 0,
          tanggal_pengerjaan: moment().format("YYYY-MM-DD HH:mm:ss"),
          now_duration: now_duration,
          end_duration: end_duration,
        });
      } else {
        console.log(sub_cat_siswa.tanggal_pengerjaan);
        sub_cat_siswa.jumlah_akses += 1;
        let up = { jumlah_akses: sub_cat_siswa.jumlah_akses };
        if (sub_cat_siswa.tanggal_pengerjaan == null) {
          up.tanggal_pengerjaan = moment().format("YYYY-MM-DD HH:mm:ss");
        }
        if (sub_cat_siswa.now_duration == null) {
          up.now_duration = now_duration;
        }
        if (sub_cat_siswa.end_duration == null) {
          up.end_duration = end_duration;
        }
        await models.ds_sub_category_in_product_siswa.update(up, {
          where: { id: sub_cat_siswa.id },
        });
      }

      sub_cat.score = sub_cat_siswa.score;
      sub_cat.now_duration = sub_cat_siswa.now_duration;
      sub_cat.end_duration = sub_cat_siswa.end_duration;
      sub_cat.jumlah_akses = sub_cat_siswa.jumlah_akses;
      sub_cat.is_done = sub_cat_siswa.is_done;
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: sub_cat,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlSubLatihanSoalSelesai: async (req, res) => {
    try {
      let { id } = req.decoded;
      let { product_id, category_id, sub_id } = req.body;
      var urlimg = req.protocol + "://" + req.get("host") + `/api/attach/`;
      let trx = await models.ds_user_transaction_product.findOne({
        where: { user_id: id, product_id: product_id, status: "success" },
      });
      if (trx == null) {
        throw new Error("Bisa Beli Dulu DI Product");
      }
      let sub_cat_siswa = await models.ds_sub_category_in_product_siswa.findOne(
        {
          where: {
            user_id: id,
            sub_id: sub_id,
            product_id: product_id,
            category_id: category_id,
            is_done: false,
          },
        }
      );
      sub_cat_siswa = JSON.parse(JSON.stringify(sub_cat_siswa));
      if (sub_cat_siswa == null) {
        throw new Error("Bisa Beli Dulu Productnya");
      }

      let f_soal = await models.sequelize
        .query(`SELECT a.type, b.user_id, b.nilai FROM
      ds_sub_category_soal_in_products AS a
      LEFT JOIN ds_user_jawabans AS b ON a.id=b.soal_id AND b.user_id=${id}
      WHERE a.product_id = ${product_id} AND a.sub_id = ${sub_id}`);
      f_soal = f_soal[0];

      let total_soal = f_soal.length;
      let total_soal_dijawab = 0;
      let score = 0;

      for (let i = 0; i < f_soal.length; i++) {
        const element = f_soal[i];
        if (element.nilai != null) {
          score += element.nilai;
        }
        if (element.type == "pilihan") {
          total_soal_dijawab++;
        }
        // else if (element.type == "essay" && element.nilai != null) {
        //   total_soal_dijawab++;
        // }
      }
      let updata = {
        tanggal_submit_pengerjaan: moment().format("YYYY-MM-DD HH:mm:ss"),
        presentase_penilaian: (total_soal_dijawab / total_soal) * 100,
        score: score,
        is_done: true,
      };
      updata.durasi_pengerjaan = moment(sub_cat_siswa.tanggal_pengerjaan).diff(
        moment(updata.tanggal_submit_pengerjaan),
        "minute"
      );
      updata.durasi_pengerjaan = Math.abs(updata.durasi_pengerjaan);
      await models.ds_sub_category_in_product_siswa.update(updata, {
        where: { id: sub_cat_siswa.id },
      });
      let total_score = await models.sequelize.query(
        `SELECT ( SUM( presentase_penilaian )/( COUNT( id )* 100 )* 100 ) AS presentase, SUM( score ) AS total_nilai, SUM( durasi_pengerjaan ) AS total_durasi_pengerjaan FROM ds_sub_category_in_product_siswas WHERE user_id=${id} AND product_id=${product_id}`
      );
      let total_score_pd = total_score[0][0].total_nilai;
      let presentase_pd = total_score[0][0].presentase;
      let total_durasi_pengerjaan = total_score[0][0].total_durasi_pengerjaan;
      await models.ds_user_transaction_product.update(
        {
          score: total_score_pd,
          presentase_penilaian: presentase_pd,
          durasi_pengerjaan: total_durasi_pengerjaan,
        },
        {
          where: {
            user_id: id,
            product_id: product_id,
          },
        }
      );
      res.status(200).json({
        responseCode: 200,
        message: "Success Edit Data",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlUserJawabanPilihan: async (req, res) => {
    try {
      let { id } = req.decoded;
      let data = req.body;
      let sub = await models.ds_sub_category_soal_in_product.findOne({
        where: {
          product_id: data.product_id,
          sub_id: data.sub_id,
        },
      });
      sub = JSON.parse(JSON.stringify(sub));
      if (sub.type != "pilihan") {
        throw new Error("Type Tidak Sesuai Hanya pilihan dan essay");
      }
      let jwb = await models.ds_user_jawaban.findOne({
        where: {
          user_id: id,
          jawaban_id: data.jawaban_id,
          product_id: data.product_id,
          sub_id: data.sub_id,
          soal_id: data.soal_id,
        },
      });
      jwb = JSON.parse(JSON.stringify(jwb));
      let jwb_data = await models.ds_sub_category_jawaban_in_product.findOne({
        where: {
          id: data.jawaban_id,
          product_id: data.product_id,
          sub_id: data.sub_id,
          soal_id: data.soal_id,
        },
      });
      jwb_data = JSON.parse(JSON.stringify(jwb_data));
      if (jwb_data == null) {
        throw new error("jawaban tidak di temukan");
      }
      let payload = {
        user_id: id,
        jawaban_id: data.jawaban_id,
        product_id: data.product_id,
        sub_id: data.sub_id,
        soal_id: data.soal_id,
        key: jwb_data.key,
        jawaban: jwb_data.jawaban,
        image: jwb_data.image,
        audio: jwb_data.audio,
        nilai: jwb_data.nilai,
        is_koreksi: true,
      };
      if (jwb == null) {
        await models.ds_user_jawaban.create(payload);
      } else {
        await models.ds_user_jawaban.update(payload, { where: { id: jwb.id } });
      }
      res.status(200).json({
        responseCode: 200,
        message: "Success Send Data",
        data: payload,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlUserJawabanEssay: async (req, res) => {
    try {
      let { id } = req.decoded;
      let data = req.body;
      let sub = await models.ds_sub_category_soal_in_product.findOne({
        where: {
          id: data.soal_id,
          product_id: data.product_id,
          sub_id: data.sub_id,
        },
      });
      sub = JSON.parse(JSON.stringify(sub));
      if (sub.type != "essay") {
        throw new Error("Type Tidak Sesuai Hanya pilihan dan essay");
      }
      let jwb = await models.ds_user_jawaban.findOne({
        where: {
          user_id: id,
          jawaban_id: 0,
          product_id: data.product_id,
          sub_id: data.sub_id,
          soal_id: data.soal_id,
        },
      });
      jwb = JSON.parse(JSON.stringify(jwb));
      let payload = {
        user_id: id,
        jawaban_id: 0,
        product_id: data.product_id,
        sub_id: data.sub_id,
        soal_id: data.soal_id,
        key: "",
        jawaban: data.jawaban,
        image: "",
        audio: "",
        nilai: 0,
      };
      if (jwb == null) {
        await models.ds_user_jawaban.create(payload);
      } else {
        await models.ds_user_jawaban.update(payload, { where: { id: jwb.id } });
      }
      res.status(200).json({
        responseCode: 200,
        message: "Success Send Data",
        data: payload,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
