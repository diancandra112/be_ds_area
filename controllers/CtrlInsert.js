const models = require("../models/index");
module.exports = {
  CtrlPdSiswa: (product_id, user_id, trx_id) => {
    return new Promise(async (resolve, reject) => {
      try {
        let product = await models.ds_sub_category_in_product.findAll({
          where: { product_id: product_id },
          include: [
            { model: models.ds_sub_category_soal_in_product, as: "soal" },
          ],
        });
        product = JSON.parse(JSON.stringify(product));
        if (product.length > 0) {
          let update = [];
          let presentase_penilaian = 0;
          let total_durasi = 0;
          for (let i = 0; i < product.length; i++) {
            const element = product[i];
            total_durasi += element.duration;
            let total_jawab = 0;
            let total_soal = element.soal.length;
            for (let ii = 0; ii < element.soal.length; ii++) {
              const element2 = element.soal[ii];
              if (element2.type == "pilihan") {
                total_jawab++;
              }
            }
            let temp_presentase_penilaian = (total_jawab / total_soal) * 100;
            presentase_penilaian += temp_presentase_penilaian;
            update.push({
              user_id: user_id,
              sub_id: element.id,
              product_id: element.product_id,
              category_id: element.category_id,
              score: 0,
              jumlah_akses: 0,
              is_done: false,
              presentase_penilaian: temp_presentase_penilaian,
              durasi_pengerjaan: element.duration,
            });
          }
          await models.ds_sub_category_in_product_siswa.bulkCreate(update);
          await models.ds_user_transaction_product.update(
            {
              durasi_pengerjaan: total_durasi,
              presentase_penilaian:
                (presentase_penilaian / (update.length * 100)) * 100,
            },
            { where: { id: trx_id } }
          );
        }
        resolve("success");
      } catch (error) {
        console.log(error);
        reject(error.message);
      }
    });
  },
};
