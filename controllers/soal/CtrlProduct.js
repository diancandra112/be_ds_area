const { Op } = require("sequelize");
const models = require("../../models/index");
const moment = require("moment");

const fs = require("fs");
var path = require("path");
var sharp = require("sharp");
module.exports = {
  CtrlEditProduct: async (req, res) => {
    try {
      let data = req.body;
      let body = {};
      let temp = [
        "nama_product",
        "harga",
        "desc",
        "benefit",
        "start_date",
        "expired_date",
      ];
      const { id } = req.params;
      temp.map((val) => {
        if (data[val] != null) {
          body[val] = data[val];
        }
      });
      if (
        data.start_date != null &&
        !moment(data.start_date, "YYYY-MM-DD HH:mm:ss", true).isValid()
      ) {
        throw new Error("Format start_date YYYY-MM-DD HH:mm:ss");
      } else if (
        data.expired_date != null &&
        !moment(data.expired_date, "YYYY-MM-DD HH:mm:ss", true).isValid()
      ) {
        throw new Error("Format expired_date YYYY-MM-DD HH:mm:ss");
      }
      if (req.file) {
        try {
          fs.access("./asset/", (error) => {
            if (error) {
              fs.mkdirSync("./asset/");
            }
          });
          let uniqueSuffix =
            (Date.now() + Math.round(Math.random() * 1e9)).toString() +
            req.file.originalname;
          await sharp(req.file.buffer)
            .webp({ quality: 10 })
            .toFile(path.resolve("./asset", uniqueSuffix));
          body.image = Buffer.from(
            uniqueSuffix + ".-splash-." + req.file.mimetype
          ).toString("base64url");
        } catch (error) {
          console.log(error);
        }
      }
      await models.ds_product.update(body, { where: { id: id } });
      res.status(200).json({
        responseCode: 200,
        message: "Success Update Data",
        data: body,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlCreateProduct: async (req, res) => {
    try {
      const data = req.body;
      if (!moment(data.start_date, "YYYY-MM-DD HH:mm:ss", true).isValid()) {
        throw new Error("Format start_date YYYY-MM-DD HH:mm:ss");
      } else if (
        !moment(data.expired_date, "YYYY-MM-DD HH:mm:ss", true).isValid()
      ) {
        throw new Error("Format expired_date YYYY-MM-DD HH:mm:ss");
      }
      let kategori = await models.ds_category.count({
        where: { id: { [Op.in]: data.category_id } },
      });
      if (kategori != data.category_id.length) {
        throw new Error("Kategori Tidak Di Temukan");
      }
      if (data.category_id.length <= 0) {
        throw new Error("Kategori Harus Ada");
      }
      if (req.file) {
        try {
          fs.access("./asset/", (error) => {
            if (error) {
              fs.mkdirSync("./asset/");
            }
          });
          let uniqueSuffix =
            (Date.now() + Math.round(Math.random() * 1e9)).toString() +
            req.file.originalname;
          await sharp(req.file.buffer)
            .webp({ quality: 10 })
            .toFile(path.resolve("./asset", uniqueSuffix));
          data.image = Buffer.from(
            uniqueSuffix + ".-splash-." + req.file.mimetype
          ).toString("base64url");
        } catch (error) {
          console.log(error);
        }
      }
      let product = await models.ds_product.create(data);
      product = JSON.parse(JSON.stringify(product));
      kategori = [];
      let total_sub = 0;
      let total_soal = 0;
      let total_durasi = 0;
      for (let i = 0; i < data.category_id.length; i++) {
        const element = data.category_id[i];
        let category = await models.ds_category.findOne({
          where: { id: element },
          attributes: [
            [models.sequelize.literal(`'${product.id}'`), "product_id"],
            "name",
            "desc",
          ],
          include: [
            {
              model: models.ds_sub_category,
              attributes: [
                [models.sequelize.literal(`'${product.id}'`), "product_id"],
                "title",
                "category_id",
                "duration",
                "rules",
              ],
              include: [
                {
                  model: models.ds_sub_category_soal,
                  attributes: [
                    [models.sequelize.literal(`'${product.id}'`), "product_id"],
                    "sub_id",
                    "no",
                    "soal",
                    "type",
                    "image",
                    "audio",
                  ],
                  include: [
                    {
                      model: models.ds_sub_category_jawaban,
                      attributes: [
                        [
                          models.sequelize.literal(`'${product.id}'`),
                          "product_id",
                        ],
                        "sub_id",
                        "soal_id",
                        "key",
                        "jawaban",
                        "nilai",
                        "image",
                        "audio",
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        });
        category = JSON.parse(JSON.stringify(category));
        let cat_temp = await models.ds_category_in_product.create(category);
        for (let i = 0; i < category.ds_sub_categories.length; i++) {
          const element = category.ds_sub_categories[i];
          element.category_id = cat_temp.id;
          total_sub += 1;
          total_durasi += element.duration;
          let sub = await models.ds_sub_category_in_product.create(element);
          sub = JSON.parse(JSON.stringify(sub));
          for (let i2 = 0; i2 < element.ds_sub_category_soals.length; i2++) {
            const element2 = element.ds_sub_category_soals[i2];
            element2.sub_id = sub.id;
            total_soal += 1;
            let sub_soal = await models.ds_sub_category_soal_in_product.create(
              element2
            );
            sub_soal = JSON.parse(JSON.stringify(sub_soal));
            let soal_jwb = [];
            element2.ds_sub_category_jawabans.map((val_soal) => {
              val_soal.sub_id = element2.sub_id;
              val_soal.soal_id = sub_soal.id;
              soal_jwb.push(val_soal);
            });
            await models.ds_sub_category_jawaban_in_product.bulkCreate(
              soal_jwb
            );
          }
        }
      }
      await models.ds_product.update(
        {
          total_sub: total_sub,
          total_soal: total_soal,
          total_durasi: total_durasi,
        },
        { where: { id: product.id } }
      );
      res.status(200).json({
        responseCode: 200,
        message: "Success Create Data",
        // data: data,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlListProduct: async (req, res) => {
    try {
      const data = req.query;
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      const { role_id, id } = req.decoded;
      var urlimg = req.protocol + "://" + req.get("host") + `/api/attach/`;
      let product = await models.ds_product.findAll({
        attributes: [
          "id",
          "having_expired",
          [
            models.sequelize.literal(
              `IF('${now}' BETWEEN start_date AND expired_date,'active','inactive') `
            ),
            "status",
          ],
          "start_date",
          ["expired_date", "end_date"],
          "nama_product",
          "harga",
          "desc",
          "benefit",
          "image",
          "is_publish",
          "publish_date",
          "createdAt",
          "updatedAt",
          "total_soal",
          // [
          //   models.sequelize.literal(
          //     `( SELECT COUNT( e.id ) FROM ds_sub_category_soal_in_products AS e WHERE ds_product.id = e.product_id )`
          //   ),
          //   "total_soal",
          // ],
          [
            models.sequelize.literal(
              `( SELECT COUNT( f.id ) FROM ds_user_transaction_products AS f WHERE ds_product.id = f.product_id AND f.status='success')`
            ),
            "total_pembelian",
          ],
        ],
        where: [data, { is_publish: false, expired_date: { [Op.gt]: now } }],
        order: [["id", "desc"]],
        include: [
          {
            model: models.ds_user_transaction_product,
            required: false,
            where: { user_id: id },
            attributes: ["id", "user_id", "mayar_link", "product_id", "status"],
          },
          {
            model: models.ds_category_in_product,
            required: true,
          },
        ],
      });
      product = JSON.parse(JSON.stringify(product));
      product.map((val) => {
        val.is_buying = false;
        val.mayar_link = null;
        if (val.ds_user_transaction_products.length > 0) {
          val.is_buying = true;
          val.mayar_link = val.ds_user_transaction_products.find(
            (vals) => (vals.product_id = val.id)
          );
          if (val.mayar_link != null) {
            val.payment_status = val.mayar_link.status;
            val.mayar_link = val.mayar_link.mayar_link;
          }
        }
        val.image = `${urlimg}${val.image}`;
        delete val.ds_user_transaction_products;
        val.category = [];
        val.ds_category_in_products.map((val2) => {
          val.category.push(val2);
        });
        delete val.ds_category_in_products;
      });
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: product,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
