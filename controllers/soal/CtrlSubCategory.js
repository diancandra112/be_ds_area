const { Op } = require("sequelize");
const models = require("../../models/index");
function parseArrayString(inputString) {
  const regex = /\[([^[\]]+)\]/g;
  const matches = inputString.match(regex);

  if (!matches) {
    return null; // String tidak sesuai format
  }

  return matches.map((match) => {
    const matchWithoutBrackets = match.slice(1, -1);
    return isNaN(matchWithoutBrackets)
      ? matchWithoutBrackets
      : parseInt(matchWithoutBrackets, 10);
  });
}

const fs = require("fs");
var path = require("path");
var sharp = require("sharp");
module.exports = {
  CtrlDetailSubCategory: async (req, res) => {
    try {
      const { id } = req.params;
      let sub_cat = await models.ds_sub_category.findOne({
        where: { id },
        include: [
          {
            model: models.ds_sub_category_soal,
            as: "soal",
            include: [{ model: models.ds_sub_category_jawaban, as: "jawaban" }],
          },
        ],
      });
      sub_cat = JSON.parse(JSON.stringify(sub_cat));
      res.status(200).json({
        responseCode: 200,
        message: "Success Create Data",
        data: sub_cat,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlCreateSubCategory: async (req, res) => {
    try {
      const data = req.body;
      if (!Array.isArray(data.bulk_file)) {
        throw new Error("bulk_file Array not found");
      }
      try {
        for (let i = 0; i < data.bulk_file.length; i++) {
          const element = data.bulk_file[i];
          let pt = Buffer.from(element, "base64url").toString();
          pt = pt.split(".-splash-.");
          pt = `./asset/${pt[0]}`;
          // await fs.unlinkSync(pt);
          await models.ds_temp_file.destroy({ where: { path: pt } });
        }
      } catch (error) {}
      let sub_flag = ["title", "category_id", "duration", "rules"];
      for (let i = 0; i < sub_flag.length; i++) {
        const element = sub_flag[i];
        if (data[element] == null) {
          throw new Error(`${element} cannot be null`);
        }
      }
      let category = await models.ds_category.findOne({
        where: { id: data.category_id },
      });
      if (category == null) {
        throw new Error("category not found");
      }
      for (let i = 0; i < data.soal.length; i++) {
        const element = data.soal[i];
        if (element.type == "essay") {
          element.jawaban = [];
        } else if (element.type == "pilihan") {
          for (let ii = 0; ii < element.jawaban.length; ii++) {
            const val = element.jawaban[ii];
            if (val.key == null || val.jawaban == null || val.nilai == null) {
              throw new Error("key, jawaban dan nilai harus di isi");
            }
          }
        } else {
          throw new Error("type soal hanya essay dan pilihan yang valid");
        }
        if (element.no == null || element.soal == null) {
          throw new Error("no dan soal harus di isi");
        }
      }

      let sub_cat = await models.ds_sub_category.create(data);
      console.log(data);
      for (let i = 0; i < data.soal.length; i++) {
        const element = data.soal[i];
        element.sub_id = sub_cat.id;
        let sub_cat_soal = await models.ds_sub_category_soal.create(element);
        element.jawaban.map((val) => {
          val.sub_id = sub_cat.id;
          val.soal_id = sub_cat_soal.id;
        });
        await models.ds_sub_category_jawaban.bulkCreate(element.jawaban);
      }
      res.status(200).json({
        responseCode: 200,
        message: "Success Create Data",
        data: data,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlEditSubCategory: async (req, res) => {
    try {
      const data = req.body;
      if (!Array.isArray(data.bulk_file)) {
        throw new Error("bulk_file Array not found");
      }
      try {
        for (let i = 0; i < data.bulk_file.length; i++) {
          const element = data.bulk_file[i];
          let pt = Buffer.from(element, "base64url").toString();
          pt = pt.split(".-splash-.");
          pt = `./asset/${pt[0]}`;
          // await fs.unlinkSync(pt);
          await models.ds_temp_file.destroy({ where: { path: pt } });
        }
      } catch (error) {}
      let sub_flag = ["title", "category_id", "duration", "rules"];
      for (let i = 0; i < sub_flag.length; i++) {
        const element = sub_flag[i];
        if (data[element] == null) {
          throw new Error(`${element} cannot be null`);
        }
      }
      let soal_lama = await models.ds_sub_category.findOne({
        where: { id: req.params.id },
      });
      soal_lama = JSON.parse(JSON.stringify(soal_lama));
      if (soal_lama == null) {
        throw new Error(`Paket Soal Tidak Di temukan`);
      }

      let category = await models.ds_category.findOne({
        where: { id: data.category_id },
      });
      if (category == null) {
        throw new Error("category not found");
      }

      for (let i = 0; i < data.soal.length; i++) {
        const element = data.soal[i];
        if (element.type == "essay") {
          element.jawaban = [];
        } else if (element.type == "pilihan") {
          for (let ii = 0; ii < element.jawaban.length; ii++) {
            const val = element.jawaban[ii];
            if (val.key == null || val.jawaban == null || val.nilai == null) {
              throw new Error("key, jawaban dan nilai harus di isi");
            }
          }
        } else {
          throw new Error("type soal hanya essay dan pilihan yang valid");
        }
        if (element.no == null || element.soal == null) {
          throw new Error("no dan soal harus di isi");
        }
      }

      await models.ds_sub_category.destroy({ where: { id: req.params.id } });
      await models.ds_sub_category_soal.destroy({
        where: { sub_id: req.params.id },
      });
      await models.ds_sub_category_jawaban.destroy({
        where: { sub_id: req.params.id },
      });
      data.id = req.params.id;
      let sub_cat = await models.ds_sub_category.create(data);
      for (let i = 0; i < data.soal.length; i++) {
        const element = data.soal[i];
        element.sub_id = sub_cat.id;
        let sub_cat_soal = await models.ds_sub_category_soal.create(element);
        element.jawaban.map((val) => {
          val.sub_id = sub_cat.id;
          val.soal_id = sub_cat_soal.id;
        });
        await models.ds_sub_category_jawaban.bulkCreate(element.jawaban);
      }
      res.status(200).json({
        responseCode: 200,
        message: "Success Edit Data",
        data: data,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlCreateSubCategoryFrom: async (req, res) => {
    try {
      const data = req.body;
      let attach = req.files;
      let sub_flag = ["title", "category_id", "duration", "rules"];
      for (let i = 0; i < sub_flag.length; i++) {
        const element = sub_flag[i];
        if (data[element] == null) {
          throw new Error(`${element} cannot be null`);
        }
      }
      let category = await models.ds_category.findOne({
        where: { id: data.category_id },
      });
      if (category == null) {
        throw new Error("category not found");
      }
      for (let i = 0; i < data.soal.length; i++) {
        const element = data.soal[i];
        if (element.type == "essay") {
          element.jawaban = [];
          element.image = "-";
          element.audio = "-";
        } else if (element.type == "pilihan") {
          element.image = "-";
          element.audio = "-";
          for (let ii = 0; ii < element.jawaban.length; ii++) {
            const val = element.jawaban[ii];
            val.image = "-";
            val.audio = "-";
            if (val.key == null || val.jawaban == null || val.nilai == null) {
              throw new Error("key, jawaban dan nilai harus di isi");
            }
          }
        } else {
          throw new Error("type soal hanya essay dan pilihan yang valid");
        }
        if (element.no == null || element.soal == null) {
          throw new Error("no dan soal harus di isi");
        }
      }
      for (let i = 0; i < attach.length; i++) {
        try {
          const element = attach[i];
          const ls = parseArrayString(element.fieldname);
          fs.access("./asset/", (error) => {
            if (error) {
              fs.mkdirSync("./asset/");
            }
          });
          let uniqueSuffix =
            (Date.now() + Math.round(Math.random() * 1e9)).toString() +
            element.originalname;
          try {
            await sharp(element.buffer)
              .webp({ quality: 10 })
              .toFile(path.resolve("./asset", uniqueSuffix));
          } catch (error) {
            await fs.writeFileSync(
              path.resolve("./asset", uniqueSuffix),
              element.buffer
            );
          }
          data.soal[ls[0]][ls[1]][ls[2]][ls[3]] = Buffer.from(
            uniqueSuffix + ".-splash-." + element.mimetype
          ).toString("base64url");
        } catch (error) {
          console.log(error);
        }
      }

      let sub_cat = await models.ds_sub_category.create(data);
      console.log(data);
      for (let i = 0; i < data.soal.length; i++) {
        const element = data.soal[i];
        element.sub_id = sub_cat.id;
        let sub_cat_soal = await models.ds_sub_category_soal.create(element);
        element.jawaban.map((val) => {
          val.sub_id = sub_cat.id;
          val.soal_id = sub_cat_soal.id;
        });
        await models.ds_sub_category_jawaban.bulkCreate(element.jawaban);
      }
      res.status(200).json({
        responseCode: 200,
        message: "Success Create Data",
        data: data,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },

  CtrlListSubCategory: async (req, res) => {
    try {
      const data = req.query;
      let subcat = await models.ds_sub_category.findAll({
        where: data,
        attributes: [
          "id",
          "title",
          "duration",
          "rules",
          [
            models.Sequelize.literal(
              "(SELECT COUNT(*) FROM ds_sub_category_soals  WHERE ds_sub_category_soals.sub_id = ds_sub_category.id)"
            ),
            "total",
          ],
        ],
        include: [
          {
            model: models.ds_category,
            attributes: ["id", "name", "desc"],
          },
        ],
      });
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: subcat });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
