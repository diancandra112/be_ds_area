const { Op } = require("sequelize");
const models = require("../../models/index");

module.exports = {
  CtrlCreateCategory: async (req, res) => {
    try {
      const data = req.body;
      let category = await models.ds_category.create(data);
      category = JSON.parse(JSON.stringify(category));
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: category,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlEditCategory: async (req, res) => {
    try {
      const { id } = req.params;
      const data = req.body;
      let user = await models.ds_category.findOne({
        where: { id: id },
        attributes: ["id"],
      });
      if (user == null) {
        throw new Error("User Not Found");
      }
      await models.ds_category.update(data, {
        where: { id: id },
      });
      res.status(200).json({ responseCode: 200, message: "Success Edit Data" });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlListCategory: async (req, res) => {
    try {
      const data = req.query;
      let user = await models.ds_category.findAll({
        where: data,
        order: [[["id", "DESC"]]],
      });
      user = JSON.parse(JSON.stringify(user));
      res.status(200).json({
        responseCode: 200,
        message: "Success Delete Data",
        data: user,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
