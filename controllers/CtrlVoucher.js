const { Op } = require("sequelize");
const models = require("../models/index");

const moment = require("moment");
module.exports = {
  CtrlEditVch: async (req, res) => {
    try {
      const data = req.body;
      const { id } = req.params;
      let update = {};
      let body = ["name", "kuota", "expired_at", "diskon"];
      body.map((val) => {
        update[val] = data[val];
      });
      await models.ds_voucher.update(update, {
        where: { id: id },
      });
      res.status(200).json({
        responseCode: 200,
        message: "Success Edit Data",
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlCreateVch: async (req, res) => {
    try {
      const data = req.body;
      if (data.name == null || data.name.length < 3) {
        throw new Error("Name Minimal memuat 3 karakter");
      } else if (data.code == null || data.code.length < 6) {
        throw new Error("code Minimal memuat 6 karakter");
      } else if (data.kuota == null || data.kuota <= 0) {
        throw new Error("kuota harus lebih dari 0");
      } else if (
        !moment(data.expired_at, "YYYY-MM-DD HH:mm:ss", true).isValid()
      ) {
        throw new Error("Format expired_at harus YYYY-MM-DD HH:mm:ss");
      } else if (data.diskon == null || data.diskon <= 0 || data.diskon > 100) {
        throw new Error(
          "diskon harus lebih dari 0 dan kurang dari sama dengan 100"
        );
      }
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      data.sisa_kuota = data.kuota;
      let vc = await models.ds_voucher.findOne({
        where: { code: data.code, expired_at: { [Op.gte]: now } },
      });
      vc = JSON.parse(JSON.stringify(vc));
      if (vc != null) {
        throw new Error("Code sudah di gunakan");
      }
      let Voucher = await models.ds_voucher.create(data);
      res.status(200).json({
        responseCode: 200,
        message: "Success Add Data",
        data: Voucher,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlListVch: async (req, res) => {
    try {
      const data = req.query;
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      let vc = await models.ds_voucher.findAll({
        where: data,
        attributes: [
          "id",
          "name",
          "code",
          "kuota",
          "sisa_kuota",
          "expired_at",
          "diskon",
          "is_unlimited",
          "createdAt",
          [
            models.sequelize.literal(
              `if(expired_at>='${now}','active','expired')`
            ),
            "status",
          ],
        ],
      });
      vc = JSON.parse(JSON.stringify(vc));
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: vc,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlVchCheck: async (req, res) => {
    try {
      const { code } = req.params;
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      let vc = await models.ds_voucher.findOne({
        where: { code: code, expired_at: { [Op.gte]: now } },
      });
      vc = JSON.parse(JSON.stringify(vc));
      if (vc == null) {
        res.status(200).json({
          responseCode: 200,
          message: "Gas Kan Bisa Di Pakai",
          data: true,
        });
      } else {
        res.status(200).json({
          responseCode: 200,
          message: "Hooooop Code Sek Di Gawe",
          data: false,
        });
      }
    } catch (error) {
      console.log(error);
      res.status(200).json({
        responseCode: 200,
        message: error.message,
        data: false,
      });
    }
  },
  CtrlVchCheckUser: async (req, res) => {
    try {
      const { code, product_id } = req.params;
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      let vc = await models.ds_voucher.findOne({
        where: {
          code: code,
          expired_at: { [Op.gte]: now },
          sisa_kuota: { [Op.gt]: 0 },
        },
      });
      vc = JSON.parse(JSON.stringify(vc));
      if (vc == null) {
        res.status(400).json({
          responseCode: 400,
          message: "Voucher Telah Habis",
          data: {},
        });
      } else if (vc.code !== code) {
        res.status(400).json({
          responseCode: 400,
          message: "Voucher Telah Habis",
          data: {},
        });
      } else {
        let product = await models.ds_product.findOne({
          where: {
            id: product_id,
          },
        });
        product = JSON.parse(JSON.stringify(product));
        let dis = (vc.diskon / 100) * product.harga;
        dis = product.harga - dis;
        if (vc.diskon < 100 && dis < 10000) {
          res.status(400).json({
            responseCode: 400,
            message: "Voucher Tidak Valid",
            data: {},
          });
        } else {
          res.status(200).json({
            responseCode: 200,
            message: "Voucher Tersedia",
            data: vc,
          });
        }
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        message: error.message,
        data: {},
      });
    }
  },
};
