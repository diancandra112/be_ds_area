const { Op } = require("sequelize");
const models = require("../models/index");

module.exports = {
  CtrlListTrx: async (req, res) => {
    try {
      const data = req.query;
      let limit = {};
      if (data.page != null && data.size != null) {
        limit = {
          offset: (parseInt(data.page) - 1) * parseInt(data.size),
          limit: parseInt(data.size),
        };
      } else if (data.limit != null) {
        limit = {
          limit: parseInt(data.limit),
        };
      }
      delete data.limit;
      delete data.page;
      delete data.size;
      const { role_id, id } = req.decoded;
      if (role_id == 3) {
        data.user_id = id;
      }
      let trx = await models.ds_user_transaction_product.findAll({
        ...limit,
        where: data,
        order: [["id", "DESC"]],
        attributes: [
          "id",
          "invoice",
          "user_id",
          "product_id",
          "status",
          "voucher",
          "discount",
          "harga",
          "total_pembayaran",
          "tanggal_transaksi",
          "tanggal_pembayaran",
          ["mayar_link", "link"],
        ],
        include: [
          { model: models.ds_user, attributes: ["id", "email", "name"] },
          { model: models.ds_product, attributes: ["id", "nama_product"] },
          { model: models.ds_voucher, required: false },
        ],
      });
      trx = JSON.parse(JSON.stringify(trx));
      trx.map((val) => {
        if (val.ds_voucher) {
          val.voucher = val.ds_voucher.name;
        }
      });
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: trx });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
