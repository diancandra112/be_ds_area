const { Op } = require("sequelize");
const moment = require("moment");
const models = require("../models/index");
const { default: axios } = require("axios");
const { CtrlPdSiswa } = require("./CtrlInsert");
require("dotenv").config();
function groupDataById(data) {
  const groupedData = {};

  data.forEach((item) => {
    const itemId = item.id;

    if (!groupedData[itemId]) {
      groupedData[itemId] = {
        id: itemId,
        image: item.image,
        expired_at: item.expired_at,
        nama_product: item.nama_product,
        harga: item.harga,
        category_name: [item.category_name],
        total_duration: item.total_duration,
        score: item.score,
        benefit: item.benefit,
        desc: item.desc,
        total_pembelian: item.total_pembelian,
        total_jawab: item.total_jawab,
        status: item.status,
        start_date: item.start_date,
        end_date: item.end_date,

        total_sub_soal_done: item.total_sub_soal_done,
        total_peserta: item.total_peserta,
        total_sub: item.total_sub,
        total_soal: item.total_soal,
        total_durasi: item.total_durasi,
        rank: item.rank,
      };
    } else {
      groupedData[itemId].category_name.push(item.category_name);
    }
  });

  const result = Object.values(groupedData);
  return result;
}
function addLeadingZeros(input) {
  const zerosToAdd = 5;
  const inputString = String(input);

  if (inputString.length >= zerosToAdd) {
    return inputString;
  } else {
    const numberOfZeros = zerosToAdd - inputString.length;
    const zeros = "0".repeat(numberOfZeros);
    return zeros + inputString;
  }
}
module.exports = {
  CtrlPaymentProduct: async (req, res) => {
    try {
      const { product_id, voucher_id, no_kontak } = req.body;
      const decoded = req.decoded;
      if (product_id == null) {
        throw new Error("product_id Tidak Bisa Kosong");
      }
      let product = await models.ds_product.findOne({
        where: { id: product_id },
      });
      product = JSON.parse(JSON.stringify(product));
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      if (product == null) {
        throw new Error("product Tidak Ditemukan");
      } else if (
        product.having_expired == false &&
        moment(product.expired_date).isSameOrBefore(moment(now))
      ) {
        throw new Error("product Telah Expired");
      }
      let trx = await models.ds_user_transaction_product.findOne({
        where: { user_id: decoded.id, product_id: product_id },
      });
      trx = JSON.parse(JSON.stringify(trx));

      let harga = product.harga;
      let total_pembayaran = harga;
      let discount = 0;
      let voucherid = 0;
      let voucher = "-";
      let sisa_kuota = 0;
      let voucher_db = null;
      if (voucher_id != null) {
        voucher_db = await models.ds_voucher.findOne({
          where: { id: voucher_id, sisa_kuota: { [Op.gt]: 0 } },
        });
        voucher_db = JSON.parse(JSON.stringify(voucher_db));
        if (voucher_db == null) {
          throw new Error("Voucher Not Valid");
        }
        discount = (voucher_db.diskon / 100) * harga;
        voucherid = voucher_id;
        voucher_db = voucher_db.name;
        total_pembayaran = harga - discount;
      }

      if (trx != null && trx.status == "success") {
        throw new Error("Product Telah DI Beli");
      } else if (trx != null && trx.status == "pending") {
        return res.status(200).json({
          responseCode: 200,
          message: "Success Get Data",
          data: trx,
        });
      } else if (total_pembayaran == 0) {
        let tgl = moment().format("YYMMDD");
        let inv = `INV${tgl}`;
        let db_trx = await models.ds_user_transaction_product.create({
          user_id: decoded.id,
          product_id: product.id,
          status: "success",
          mayar_id: 0,
          mayar_transaction_id: 0,
          mayar_link: "https://dsarea-tryout.vercel.app/siswa/latihan-soal",
          voucher_id: voucherid,
          voucher: voucher,
          discount: discount,
          harga: harga,
          total_pembayaran: total_pembayaran,
          tanggal_transaksi: now,
          tahun_transaksi: moment(now).format("YYYY"),
          periode_transaksi: moment(now).format("YYYY-MM"),
        });
        db_trx = JSON.parse(JSON.stringify(db_trx));
        inv = `${inv}${addLeadingZeros(db_trx.id)}`;
        db_trx.invoice = inv;
        await models.ds_user_transaction_product.update(
          {
            invoice: inv,
          },
          { where: { id: db_trx.id } }
        );
        await CtrlPdSiswa(product_id, decoded.id, db_trx.id);
        await models.ds_notif.bulkCreate([
          {
            title: "Pembayaran Berhasil",
            body: `Pembelian Product Dengan Invoice ${inv} Berhasil Dilakukan`,
            tanggal: moment().format("YYYY-MM-DD HH:mm:ss"),
            product_id: product_id,
            user_id: decoded.id,
            trx_id: db_trx.id,
          },
          {
            title: "Pembayaran Berhasil",
            body: `Pembelian Product Dengan Invoice ${inv} Berhasil Dilakukan`,
            tanggal: moment().format("YYYY-MM-DD HH:mm:ss"),
            product_id: product_id,
            user_id: 0,
            trx_id: db_trx.id,
          },
        ]);
        return res.status(200).json({
          responseCode: 200,
          message: "Success Get Data",
          data: db_trx,
        });
      } else {
        let tgl = moment().format("YYMMDD");
        let inv = `INV${tgl}`;
        let send = await axios({
          method: "POST",
          baseURL: process.env.mayar_baseurl,
          url: "/payment/create",
          headers: { Authorization: process.env.mayar_token },
          data: {
            name: decoded.name,
            email: decoded.email,
            amount: total_pembayaran,
            mobile: no_kontak,
            redirectUrl: "https://dsarea-tryout.vercel.app/siswa/latihan-soal",
            description: `Pembelian Product\n ${product.nama_product}`,
          },
        });
        send = send.data.data;
        let db_trx = await models.ds_user_transaction_product.create({
          user_id: decoded.id,
          product_id: product.id,
          status: "pending",
          mayar_id: send.id,
          mayar_transaction_id: send.transaction_id,
          mayar_link: send.link,
          voucher_id: voucherid,
          voucher: voucher,
          discount: discount,
          harga: harga,
          total_pembayaran: total_pembayaran,
          tanggal_transaksi: now,
          tahun_transaksi: moment(now).format("YYYY"),
          periode_transaksi: moment(now).format("YYYY-MM"),
          tanggal_pembayaran: now,
        });
        db_trx = JSON.parse(JSON.stringify(db_trx));
        inv = `${inv}${addLeadingZeros(db_trx.id)}`;
        db_trx.invoice = inv;
        await models.ds_user_transaction_product.update(
          {
            invoice: inv,
          },
          { where: { id: db_trx.id } }
        );
        if (voucher_db != null) {
          await models.ds_voucher.update(
            { sisa_kuota: sisa_kuota.sisa_kuota + 1 },
            {
              where: { id: voucher_id },
            }
          );
        }
        await CtrlPdSiswa(product_id, decoded.id, db_trx.id);
        await models.ds_notif.bulkCreate([
          {
            title: "Pembayaran Tertunda",
            body: `Segera Lakukan Pembayaran Untuk Pembelian Product Dengan Invoice ${inv}`,
            tanggal: moment().format("YYYY-MM-DD HH:mm:ss"),
            product_id: product_id,
            user_id: decoded.id,
            trx_id: db_trx.id,
            link: send.link,
          },
          {
            title: "Pembayaran Tertunda",
            body: `Pembelian Product Dengan Invoice ${inv}`,
            tanggal: moment().format("YYYY-MM-DD HH:mm:ss"),
            product_id: product_id,
            user_id: 0,
            trx_id: db_trx.id,
            link: send.link,
          },
        ]);
        return res.status(200).json({
          responseCode: 200,
          message: "Success Get Data",
          data: db_trx,
        });
      }
    } catch (error) {
      console.log(error);
      if (error.response) {
        return res.status(400).json(error.response.data);
      }
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlDetailProductPembelian: async (req, res) => {
    try {
      const { id } = req.params;
      const decoded = req.decoded;
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      var urlimg = req.protocol + "://" + req.get("host") + `/api/attach/`;
      let user = await models.sequelize.query(`SELECT
      b.id,
      b.benefit,
      b.desc,
      b.harga,
      CONCAT('${urlimg}',b.image) AS image,
      IF('${now}' BETWEEN start_date AND expired_date,'active','inactive') AS status,
      b.start_date,
      b.expired_date AS  end_date,
      b.nama_product,
      c.NAME AS category_name,
      ( SELECT SUM( d.duration ) FROM ds_sub_category_in_products AS d WHERE b.id = d.product_id ) AS total_duration,
      ( SELECT COUNT( e.id ) FROM ds_sub_category_soal_in_products AS e WHERE b.id = e.product_id ) AS total_soal,
      ( SELECT COUNT( f.id ) FROM ds_user_transaction_products AS f WHERE b.id = f.product_id ) AS total_pembelian,
      "-" AS score 
    FROM
      ds_products AS b
      INNER JOIN ds_category_in_products AS c ON b.id = c.product_id 
    WHERE
      b.id =${id}`);
      user = user[0];
      user = groupDataById(user);

      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: user });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlDetailUserProduct: async (req, res) => {
    try {
      let { id } = req.params;
      if (id == "owned") {
        id = req.decoded.id;
      }
      const data = req.query;
      let filter = "";
      const now = moment().format("YYYY-MM-DD HH:mm:ss");
      if (data.status == "selesai") {
        filter = `AND b.is_publish = 1`;
      } else if (data.status == "active") {
        filter = `AND b.is_publish = 0 AND b.expired_date >= '${now}'`;
      } else if (data.status == "expired") {
        filter = `AND b.is_publish = 0 AND b.expired_date < '${now}'`;
      }
      let limit = "";
      if (data.limit) {
        limit = `LIMIT ${data.limit}`;
      }
      var urlimg = req.protocol + "://" + req.get("host") + `/api/attach/`;
      let user = await models.sequelize.query(`SELECT
      b.id,
      b.benefit,
      b.desc,
      CONCAT('${urlimg}',b.image) AS image,
      expired_date AS expired_at,
      b.nama_product,
      b.harga,
      c.NAME AS category_name,
      ( SELECT COUNT( g.id ) FROM ds_sub_category_in_product_siswas AS g WHERE b.id = g.product_id AND g.is_done=1 AND g.user_id=${id} ) AS total_sub_soal_done,
      b.total_sub,
      b.total_soal,
      b.total_durasi,
      a.rank,
      a.score,
      ( SELECT COUNT( g.id ) FROM ds_user_transaction_products AS g WHERE b.id = g.product_id AND g.status='success' ) AS total_peserta,
      CASE
        WHEN b.is_publish = 1 THEN "selesai"
        WHEN b.is_publish = 0 AND b.expired_date >= '${now}' THEN 'active'
        ELSE 'expired'
      END AS status
    FROM
    ds_user_transaction_products AS a
      INNER JOIN ds_products AS b ON a.product_id = b.id
      INNER JOIN ds_category_in_products AS c ON b.id = c.product_id 
    WHERE
      a.user_id =${id} AND a.status='success' ${filter} ${limit}`);
      user = user[0];
      user = groupDataById(user);

      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: user });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlDsCalender: async (req, res) => {
    try {
      let { tanggal } = req.query;
      let { id } = req.decoded;
      if (!moment(tanggal, "YYYY-MM-DD HH:mm:ss", true).isValid()) {
        throw new Error("Invalid Format tanggal Harus YYYY-MM-DD HH:mm:ss");
      }
      let year = moment(tanggal).format("YYYY");
      let month = moment(tanggal).format("MM");
      let user = await models.sequelize.query(`
      SELECT b.id, b.nama_product AS title, b.start_date AS 'start', b.expired_date AS 'end' 
      FROM ds_user_transaction_products AS a
	    INNER JOIN ds_products AS b ON a.product_id = b.id 
      WHERE a.user_id = ${id} AND a.status = 'success' AND YEAR(b.expired_date)='${year}' AND MONTH(b.expired_date)='${month}'
      `);
      user = user[0];

      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: user });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlListUserRole: async (req, res) => {
    try {
      const data = req.query;
      if (data.role_id != null) {
        data.role_id = { [Op.in]: data.role_id.split(",") };
      }
      let user = await models.ds_user.findAll({
        where: data,
        attributes: [
          "id",
          "user_id",
          "email",
          "verified_email",
          "name",
          "given_name",
          "picture",
          "role_id",
          [
            models.sequelize.literal(
              `IF(last_access IS NULL OR TIMESTAMPDIFF(DAY,last_access,NOW())>=30,'inactive','active')`
            ),
            "status",
          ],
          "last_access",
        ],
        include: [{ model: models.ds_user_role, attributes: ["id", "name"] }],
      });
      user = JSON.parse(JSON.stringify(user));
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: user });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlChangeUserRole: async (req, res) => {
    try {
      const data = req.body;
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
        if (element.id == null || element.role_id == null) {
          throw new Error("Invalid Input");
        }
      }
      if (data.length < 0) {
        throw new Error("No Data Update");
      }
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
        await models.ds_user.update(
          { role_id: element.role_id },
          {
            where: { id: element.id },
          }
        );
      }
      res.status(200).json({ responseCode: 200, message: "Success Edit Data" });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlListRole: async (req, res) => {
    try {
      const data = req.query;
      let user = await models.ds_user_role.findAll({
        where: data,
        attributes: ["id", "name"],
      });
      user = JSON.parse(JSON.stringify(user));
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: user });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlProfile: async (req, res) => {
    try {
      const data = req;
      let user = await models.ds_user.findOne({
        where: { id: data.decoded.id },
        include: [{ model: models.ds_user_role, attributes: ["id", "name"] }],
      });
      user = JSON.parse(JSON.stringify(user));
      res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: user });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
