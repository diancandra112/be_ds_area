const { Op } = require("sequelize");
const moment = require("moment");
const models = require("../models/index");
function diffpersen(sebelum, sekarang) {
  if (sebelum == 0) {
    return "100%";
  }
  let selisih = sekarang - sebelum;
  let persen = (selisih / sebelum) * 100;
  return `${persen}%`;
}
module.exports = {
  CtrlOverview: async (req, res) => {
    try {
      const data = req.query;
      let ds_category = await models.ds_category.count();
      ds_category = JSON.parse(JSON.stringify(ds_category));
      let ds_sub_category = await models.ds_sub_category.count();
      ds_sub_category = JSON.parse(JSON.stringify(ds_sub_category));
      let ds_user = await models.sequelize.query(`
      SELECT COUNT(*) AS jumlah, IF(last_access IS NULL OR TIMESTAMPDIFF(DAY, last_access, NOW()) >= 30, 'inactive', 'active') AS status_user
      FROM ds_users WHERE role_id=3 GROUP BY status_user`);
      ds_user = ds_user[0];
      let inactive = ds_user.find((val) => val.status_user == "inactive");
      let active = ds_user.find((val) => val.status_user == "active");
      active = active ? active.jumlah : 0;
      inactive = inactive ? inactive.jumlah : 0;
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: {
          total_kategori: ds_category,
          total_sub_kategori: ds_sub_category,
          total_siswa: active + inactive,
          total_siswa_active: active,
          total_siswa_inactive: inactive,
        },
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlTransaksi: async (req, res) => {
    try {
      const { tahun } = req.params;
      let template = [];
      for (let i = 1; i <= 12; i++) {
        let bln = ("00" + i).toString().slice(-2);
        template.push({ date: `${tahun}-${bln}`, value: 0 });
      }
      let data_year = moment().subtract(1, "year").format("YYYY");
      let tahun_sebelum = await models.ds_yearly.findOne({
        where: { year: data_year },
      });
      let total_gross_sebelum = 0;
      let total_net_sebelum = 0;
      let total_pending_sebelum = 0;
      let total_failed_sebelum = 0;
      if (tahun_sebelum != null) {
        total_gross_sebelum = tahun_sebelum.gross;
        total_net_sebelum = tahun_sebelum.net;
        total_pending_sebelum = tahun_sebelum.pending;
        total_failed_sebelum = tahun_sebelum.failed;
      }
      let data = await models.sequelize
        .query(`SELECT SUM(harga) as gross,SUM(total_pembayaran) as nett,
      status,periode_transaksi FROM ds_user_transaction_products
      WHERE tahun_transaksi='${tahun}' GROUP BY status,periode_transaksi
      ORDER BY periode_transaksi ASC`);
      data = data[0];
      let total_gross = 0;
      let total_net = 0;
      let total_pending = 0;
      let total_failed = 0;
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
        if (element.status == "success") {
          total_gross += element.gross;
          total_net += element.nett;
          let findidx = template.findIndex(
            (val) => val.date == element.periode_transaksi
          );
          if (findidx >= 0) {
            template[findidx].value += element.nett;
          }
        } else if (element.status == "pending") {
          total_pending += element.nett;
        } else if (element.status == "failed") {
          total_failed += element.nett;
        }
      }
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: {
          total_gross,
          total_gross_persen: diffpersen(total_gross_sebelum, total_gross),
          total_net,
          total_net_persen: diffpersen(total_net_sebelum, total_net),
          total_pending,
          total_pending_persen: diffpersen(
            total_pending_sebelum,
            total_pending
          ),
          total_failed,
          total_failed_persen: diffpersen(total_failed_sebelum, total_failed),
          grafik: template,
        },
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlListProdcutDs: async (req, res) => {
    try {
      const search = req.query;
      const { id } = req.decoded;
      let data = await models.ds_product.findAll({
        where: { is_publish: true },
        attributes: ["id", "nama_product"],
        include: [
          {
            model: models.ds_user_transaction_product,
            required: true,
            attributes: ["id", "user_id", "status"],
            where: { status: "success", user_id: id },
          },
        ],
      });
      data = JSON.parse(JSON.stringify(data));
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: data,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlListPemenang: async (req, res) => {
    try {
      const { product_id } = req.params;
      const { id } = req.decoded;
      let data = await models.ds_product.findAll({
        where: { is_publish: true, id: product_id },
        attributes: ["id", "nama_product"],
      });
      data = JSON.parse(JSON.stringify(data));
      if (data == null) {
        throw new Error(`Product id ${product_id} tidak di temukan `);
      }
      let pemenang_all = await models.sequelize.query(`
      SELECT
	a.rank,
	b.email,
	a.score 
FROM
	ds_user_transaction_products AS a
	INNER JOIN ds_users AS b ON a.user_id = b.id 
WHERE
	a.product_id = ${product_id} 
	AND a.status = 'success' 
ORDER BY
	a.rank
      `);
      pemenang_all = pemenang_all[0];
      let myrank = await models.sequelize.query(`
      SELECT
	a.rank,
	b.email,
	a.score 
FROM
	ds_user_transaction_products AS a
	INNER JOIN ds_users AS b ON a.user_id = b.id 
WHERE
	a.product_id = ${product_id} 
	AND a.status = 'success' 
  AND a.user_id = ${id}
ORDER BY
	a.rank
      `);
      myrank = myrank[0];
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: { all: pemenang_all, my_rank: myrank },
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
