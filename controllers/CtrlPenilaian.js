const { Op } = require("sequelize");
const moment = require("moment");
const models = require("../models/index");
function diffpersen(sebelum, sekarang) {
  if (sebelum == 0) {
    return "100%";
  }
  let selisih = sekarang - sebelum;
  let persen = (selisih / sebelum) * 100;
  return `${persen}%`;
}
function groupByUserId(data, total) {
  let groupedData = {};

  // Iterate through each item in the data array
  data.forEach((item) => {
    // If the user_id is already a key in groupedData, add the item to its corresponding array
    if (groupedData[item.user_id]) {
      groupedData[item.user_id].category.push({
        user_id: item.user_id,
        usr_id: item.usr_id,
        product_id: item.product_id,
        category_id: item.category_id,
        sub_id: item.sub_id,
        category_title: item.category_title,
        category_score: item.category_score,
        category_presentase_penilaian: item.category_presentase_penilaian,
      });
    } else {
      // If user_id is not a key in groupedData, create a new entry for it
      groupedData[item.user_id] = {
        is_publish: item.is_publish,
        total_soal: total,
        user_id: item.user_id,
        start_date: item.start_date,
        expired_date: item.expired_date,
        name: item.name,
        score: item.score,
        presentase_penilaian: item.presentase_penilaian,
        category: [
          {
            user_id: item.user_id,
            usr_id: item.usr_id,
            product_id: item.product_id,
            category_id: item.category_id,
            sub_id: item.sub_id,
            category_title: item.category_title,
            category_score: item.category_score,
            category_presentase_penilaian: item.category_presentase_penilaian,
          },
        ],
      };
    }
  });

  // Convert the groupedData object into an array of values
  return Object.values(groupedData);
}
module.exports = {
  CtrlDetailPenilaian: async (req, res) => {
    try {
      let { product_id } = req.params;
      let list = await models.sequelize.query(`
      SELECT
	usr.id AS user_id,
  sub_siswa.user_id AS usr_id,
	sub_siswa.product_id,
	sub_siswa.category_id,
	sub_siswa.sub_id,
	prd.start_date,
	prd.expired_date,
	usr.name,
  prd.is_publish,
	trx.score,
	trx.presentase_penilaian,
	sub.title AS category_title,
	sub_siswa.score AS category_score,
	sub_siswa.presentase_penilaian AS category_presentase_penilaian 
FROM
	ds_user_transaction_products AS trx
	INNER JOIN ds_products AS prd ON prd.id = trx.product_id
	INNER JOIN ds_users AS usr ON usr.id = trx.user_id
	INNER JOIN ds_sub_category_in_products AS sub ON sub.product_id = trx.product_id
	LEFT JOIN ds_sub_category_in_product_siswas AS sub_siswa ON sub_siswa.sub_id = sub.id AND sub_siswa.user_id = trx.user_id
WHERE
	trx.product_id = ${product_id} 
	AND trx.status = 'success' 
GROUP BY
	usr.id,
  sub_siswa.user_id,
	sub_siswa.product_id,
	sub_siswa.category_id,
  prd.is_publish,
	sub_siswa.sub_id,
	prd.start_date,
	prd.expired_date,
	usr.name,
	trx.score,
	trx.presentase_penilaian,
	sub.title,
	sub_siswa.score,
	sub_siswa.presentase_penilaian 
ORDER BY
	usr.id ASC
      `);
      list = list[0];
      let total_soal = await models.sequelize.query(`
   SELECT COUNT( b.id ) AS 'total_soal' FROM ds_sub_category_soal_in_products AS b WHERE b.product_id = ${product_id} 
      `);
      total_soal = total_soal[0][0].total_soal;
      const groupedData = groupByUserId(list, total_soal);
      let jumlah_data = groupedData.length;
      let jumlah_data_100 = groupedData.filter(
        (val) => val.presentase_penilaian == 100
      ).length;
      let nilai = `${jumlah_data_100} dari ${jumlah_data} telah di nilai`;
      groupedData.map((val) => {
        val.jumlah_penilaian = nilai;
      });
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: groupedData,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlListPenilaian: async (req, res) => {
    try {
      const data = req.query;
      let now = moment().format("YYYY-MM-DD HH:mm:ss");
      let ds_user = await models.sequelize.query(`
      SELECT
      a.id AS product_id,
      a.nama_product,
      a.start_date,
      a.expired_date,
      a.is_publish,
      a.publish_date,
      COUNT( trx.id ) AS 'total_siswa',
      SUM( trx.presentase_penilaian ) AS 'persentase_koreksi',
      ( SELECT COUNT( b.id ) FROM ds_sub_category_soal_in_products AS b WHERE b.product_id = a.id ) AS 'total_soal'
    FROM
    ds_products AS a
    INNER JOIN ds_user_transaction_products AS trx ON trx.product_id = a.id AND trx.status = 'success'
    WHERE a.expired_date<='${now}'
    GROUP BY a.id
      `);
      ds_user = ds_user[0];
      ds_user.map((val) => {
        val.persentase_koreksi =
          (val.persentase_koreksi / (100 * val.total_siswa)) * 100;
        val.is_publish = val.is_publish == 1 ? true : false;
      });
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: ds_user,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlSoalPenilaian: async (req, res) => {
    try {
      let { product_id, category_id, sub_id, user_id } = req.params;
      var urlimg = req.protocol + "://" + req.get("host") + `/api/attach/`;
      let trx = await models.ds_user_transaction_product.findOne({
        where: { user_id: user_id, product_id: product_id, status: "success" },
      });
      if (trx == null) {
        throw new Error("Bisa Beli Dulu DI Product");
      }
      // let sub_cat_siswa = await models.ds_sub_category_in_product_siswa.findOne(
      //   {
      //     where: {
      //       user_id: user_id,
      //       sub_id: sub_id,
      //       product_id: product_id,
      //       category_id: category_id,
      //     },
      //   }
      // );
      let sub_cat = await models.ds_sub_category_in_product.findOne({
        attributes: [
          "id",
          ["title", "sub_category_name"],
          "product_id",
          "category_id",
          "duration",
          "rules",
          "createdAt",
          "updatedAt",
        ],
        where: {
          id: sub_id,
          product_id: product_id,
          category_id: category_id,
        },
        include: [
          {
            model: models.ds_sub_category_soal_in_product,
            as: "soal",
            attributes: [
              ["id", "soal_id"],
              "product_id",
              "sub_id",
              "no",
              "soal",
              "type",
              [
                models.Sequelize.fn(
                  "CONCAT",
                  models.Sequelize.literal(`'${urlimg}'`),
                  models.Sequelize.col("soal.image")
                ),
                "image",
              ],
              [
                models.Sequelize.fn(
                  "CONCAT",
                  models.Sequelize.literal(`'${urlimg}'`),
                  models.Sequelize.col("soal.audio")
                ),
                "audio",
              ],
            ],
            include: [
              {
                required: false,
                model: models.ds_sub_category_jawaban_in_product,
                attributes: [
                  ["id", "jawaban_id"],
                  "nilai",
                  "product_id",
                  "sub_id",
                  "soal_id",
                  "key",
                  "jawaban",
                  [
                    models.Sequelize.fn(
                      "CONCAT",
                      models.Sequelize.literal(`'${urlimg}'`),
                      models.Sequelize.col("soal.jawaban.image")
                    ),
                    "image",
                  ],
                  [
                    models.Sequelize.fn(
                      "CONCAT",
                      models.Sequelize.literal(`'${urlimg}'`),
                      models.Sequelize.col("soal.jawaban.audio")
                    ),
                    "audio",
                  ],
                ],
                as: "jawaban",
              },
              {
                required: false,
                model: models.ds_user_jawaban,
                as: "jawaban_user",
                attributes: [
                  ["id", "id_user_jawab"],
                  "nilai",
                  "user_id",
                  "jawaban_id",
                  "product_id",
                  "sub_id",
                  "soal_id",
                  "key",
                  "jawaban",
                  "image",
                  "audio",
                ],
                where: { user_id: user_id },
              },
            ],
          },
        ],
      });
      sub_cat = JSON.parse(JSON.stringify(sub_cat));
      let soal = 0;
      let soal_jawab = 0;
      sub_cat.soal.map((val) => {
        if (val.jawaban_user != null) {
          soal_jawab++;
        }
        soal++;
      });
      sub_cat.terjawab = soal_jawab;
      sub_cat.dilewati = soal - soal_jawab;
      sub_cat.total_pertanyaan = soal;
      let category = await models.ds_category_in_product.findOne({
        where: { id: sub_cat.category_id },
      });
      sub_cat.category_name = category.name;
      let sub_category_siswa =
        await models.ds_sub_category_in_product_siswa.findOne({
          where: {
            user_id: user_id,
            sub_id: sub_id,
            product_id: product_id,
            category_id: category_id,
          },
        });
      let user = await models.ds_user.findOne({
        where: {
          id: user_id,
        },
      });
      sub_cat.nama_siswa = user.name;
      sub_cat.email_siswa = user.email;

      sub_cat.skor = 0;
      sub_cat.tanggal_pengerjaan = null;
      if (sub_category_siswa != null) {
        sub_cat.skor = sub_category_siswa.score;
        sub_cat.tanggal_pengerjaan = sub_category_siswa.tanggal_pengerjaan;
      }
      for (let i = 0; i < sub_cat.soal.length; i++) {
        const element = sub_cat.soal[i];
        if (element.type == "essay" && element.jawaban_user == null) {
          let jawab = await models.ds_user_jawaban.create({
            user_id: user_id,
            jawaban_id: 0,
            product_id: element.product_id,
            sub_id: element.sub_id,
            soal_id: element.soal_id,
            key: "",
            jawaban: "",
            image: "",
            audio: "",
            nilai: 0,
            is_koreksi: true,
          });
          jawab = JSON.parse(JSON.stringify(jawab));
          jawab.id_user_jawab = jawab.id;
          delete jawab.id;
          sub_cat.soal[i].jawaban_user = jawab;
        }
      }
      res.status(200).json({
        responseCode: 200,
        message: "Success Get Data",
        data: sub_cat,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlPublishPenilaian: async (req, res) => {
    try {
      let { product_id } = req.params;
      let trx = await models.ds_user_transaction_product.findAll({
        where: { product_id: product_id, status: "success" },
        attributes: ["id", "product_id", "user_id", "score"],
        order: [["score", "DESC"]],
      });
      trx = JSON.parse(JSON.stringify(trx));
      let product = await models.ds_product.findOne({
        where: { id: product_id },
      });
      product = JSON.parse(JSON.stringify(product));
      let notif = [
        {
          title: "Pengumuman Nilai",
          body: `Publish Nilai telah dilakukan pada Product ${product.nama_product}`,
          tanggal: moment().format("YYYY-MM-DD HH:mm:ss"),
          product_id: product_id,
          user_id: 0,
          trx_id: 0,
        },
      ];
      for (let i = 0; i < trx.length; i++) {
        const element = trx[i];
        await models.ds_user_transaction_product.update(
          { rank: i + 1 },
          {
            where: { id: element.id },
          }
        );
        notif.push({
          title: "Pengumuman Nilai",
          body: `Penilaian telah dilakukan pada ${product.nama_product} anda bisa melihat nilai anda disini!`,
          tanggal: moment().format("YYYY-MM-DD HH:mm:ss"),
          product_id: product_id,
          user_id: element.user_id,
          trx_id: element.id,
        });
      }
      await models.ds_notif.bulkCreate(notif);
      await models.ds_product.update(
        {
          is_publish: true,
          publish_date: moment().format("YYYY-MM-DD HH:mm:ss"),
        },
        {
          where: { id: product_id },
        }
      );
      res.status(200).json({
        responseCode: 200,
        message: "Success Publish Data",
        // trx,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlSubmitPenilaian: async (req, res) => {
    try {
      const data = req.body;
      for (let id = 0; id < data.length; id++) {
        const element = data[id];
        if (element.id_user_jawab == null || isNaN(element.id_user_jawab)) {
          throw new Error("Invalid Input id_user_jawab");
        } else if (element.nilai == null || isNaN(element.nilai)) {
          throw new Error("Invalid Input nilai");
        }
      }
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
        await models.ds_user_jawaban.update(
          { nilai: element.nilai, is_koreksi: true },
          {
            where: { id: element.id_user_jawab },
          }
        );
      }
      if (data.length > 0) {
        let f_data = await models.ds_user_jawaban.findOne({
          where: { id: data[0].id_user_jawab },
        });
        f_data = JSON.parse(JSON.stringify(f_data));
        let f_soal = await models.sequelize
          .query(`SELECT a.type, b.user_id, b.nilai FROM
          ds_sub_category_soal_in_products AS a 
          LEFT JOIN ds_user_jawabans AS b ON a.id=b.soal_id AND b.user_id=${f_data.user_id}
          WHERE a.product_id = ${f_data.product_id} AND a.sub_id = ${f_data.sub_id}`);
        f_soal = f_soal[0];
        let total_soal = f_soal.length;
        let total_soal_dijawab = 0;
        let score = 0;
        for (let i = 0; i < f_soal.length; i++) {
          const element = f_soal[i];
          if (element.nilai != null) {
            score += element.nilai;
          }
          if (element.type == "pilihan") {
            total_soal_dijawab++;
          } else if (element.type == "essay" && element.nilai != null) {
            total_soal_dijawab++;
          }
        }
        let update = {
          score: score,
          presentase_penilaian: (total_soal_dijawab / total_soal) * 100,
        };
        await models.ds_sub_category_in_product_siswa.update(update, {
          where: {
            user_id: f_data.user_id,
            sub_id: f_data.sub_id,
            product_id: f_data.product_id,
          },
        });

        let total_score = await models.sequelize.query(
          `SELECT ( SUM( presentase_penilaian )/( COUNT( id )* 100 )* 100 ) AS presentase, SUM( score ) AS total_nilai FROM ds_sub_category_in_product_siswas WHERE user_id=${f_data.user_id} AND product_id=${f_data.product_id}`
        );
        let total_score_pd = total_score[0][0].total_nilai;
        let presentase_pd = total_score[0][0].presentase;

        await models.ds_user_transaction_product.update(
          {
            score: total_score_pd,
            presentase_penilaian: presentase_pd,
          },
          {
            where: {
              user_id: f_data.user_id,
              product_id: f_data.product_id,
            },
          }
        );
      }
      res.status(200).json({
        responseCode: 200,
        message: "Success Set Data",
        data: data,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};
