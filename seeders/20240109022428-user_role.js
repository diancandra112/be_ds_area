"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "ds_user_roles",
      [
        {
          id: 1,
          name: "super admin",
        },
        {
          id: 2,
          name: "admin",
        },
        {
          id: 3,
          name: "siswa",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("People", null, {});
  },
};
